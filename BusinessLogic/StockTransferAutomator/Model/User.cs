﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator.Model
{
    public class User
    {
        private static string mLoginName;
        private static string mLoginPassword;
        private static string mLoginCompany;
        private static string mLoginServer;
        private static string mgblLSessionID;
        private static string mName;
        private static string mOffice;
        private static string mScreen;
        private static string mDescription;
        public static int UpdatePrice;
        private static string mDummySetting = string.Empty;
        private static DataTable mDataTable = new DataTable();
        private static DataTable rDataTable = new DataTable();
        private static List<string> mLabFeatures;
        public static string Email;
        private static string mADUsr;
        private static List<string> repNo;
        private static List<string> repNames;

        public static bool isKBinit = false;
        public static bool isTesting = false;
        public static bool isITuser = false;
        public static string nusr = "";
        public static string emailaddr = "";
        public static string displayname = "";


        /// <summary>
        /// Get/set Screen Menu.
        /// </summary>
        public static string Screen
        {
            get { return mScreen; }
            set { mScreen = value; }
        }

        /// <summary>
        /// Get or set Use Master status.
        /// </summary>
        public static bool IsUseMaster { get; set; }

        /// <summary>
        /// Get or set LoginName
        /// </summary>
        public static string LoginName
        {
            get { return mLoginName; }
            set { mLoginName = value; }
        }
        /// <summary>
        /// Get or set Name
        /// </summary>
        public static string Name
        {
            get { return mName; }
            set { mName = value; }
        }

        /// <summary>
        /// Get or set Office
        /// </summary>
        public static string Office
        {
            get { return mOffice; }
            set { mOffice = value; }
        }
        /// <summary>
        /// Get or set LoginCompany
        /// </summary>
        public static string LoginCompany
        {
            get { return mLoginCompany; }
            set { mLoginCompany = value; }
        }

        /// <summary>
        /// Get or set LoginServer
        /// </summary>
        public static string LoginServer
        {
            get { return mLoginServer; }
            set { mLoginServer = value; }
        }

        /// <summary>
        /// Get or set LoginPassword
        /// </summary>
        public static string LoginPassword
        {
            get { return mLoginPassword; }
            set { mLoginPassword = value; }
        }

        /// <summary>
        /// Get or set Active Directory Username
        /// </summary>
        public static string ADUser
        {
            get { return mADUsr; }
            set { mADUsr = value; }
        }

        // global session ID for user till they logoff - added by Malin

        public static string gblLSessionID
        {
            get { return mgblLSessionID; }
            set { mgblLSessionID = value; }
        }

        /// <summary>
        /// Get or set Description
        /// </summary>
        public static string Description
        {
            get { return mDescription; }
            set { mDescription = value; }
        }

        public string DummySetting
        {
            set { mDummySetting = value; }
        }

        /// <summary>
        /// Retrieves the description for the logged in user. (This is normally their department.)
        /// </summary>
        //private static string GetDescription()
        //{
        //    if (mDescription == null)
        //    {
        //        string sql = "SELECT description FROM [user] WHERE usr ='" + mLoginName + "'";
        //        mDescription = Database.GetValue("NETCRM", sql, "description");
        //    }

        //    return mDescription;
        //}



        //        public SqlConnection cnn = DataSource.getConnFromString
        //                                     ("Application Name=NetCRM-" + ";Server=NetCRMAU;Database=NetCRM;uid=dev;Packet Size=4096;");

        //        public string DummySetting
        //        {
        //            set { mDummySetting = value; }
        //        }

        //        public void initKB()
        //        {
        //            PermissionReader.isKBinit = true;
        //            DataTable tbITUser = DataSource.GetTableFrom(@"select * from KBgroup where  dusr = @usr ", cnn, new object[,] { { "usr", PermissionReader.ADUser } });
        //            if (tbITUser != null && tbITUser.Rows.Count > 0)
        //            {
        //                PermissionReader.isITuser = true;
        //                DataSource.isITuser = true;
        //            }
        //            PermissionReader.nusr = PermissionReader.LoginName;
        //            PermissionReader.emailaddr = PermissionReader.Email;
        //            DataTable dtcurrentusr = DataSource.GetTableFrom(@"select * from usrmap a left outer join vusers b on (a.nusr = b.usr) where nusr = @usr "
        //                , cnn, new object[,] { { "usr", PermissionReader.LoginName } });
        //            if (dtcurrentusr != null && dtcurrentusr.Rows.Count > 0)
        //            {

        //                //nusr = dtcurrentusr.Rows[0]["nusr"].ToString().Trim();
        //                //emailaddr = dtcurrentusr.Rows[0]["email"].ToString().Trim();
        //                PermissionReader.displayname = dtcurrentusr.Rows[0]["full_name"].ToString().Trim();
        //                DataSource.sOffice = dtcurrentusr.Rows[0]["office"].ToString().Trim();
        //                //if (DataSource.isLoadFrmDesktop == true) PermissionReader.LoginName = nusr;

        //            }
        //            //PermissionReader.displayname = PermissionReader.fu

        //        }
        //        public void RetrieveRole(string UserName)
        //        {
        //            rDataTable.Clear();
        //            NETCRM ln = new NETCRM();
        //            SqlCommand com = new SqlCommand();
        //            try
        //            {
        //                System.DateTime tsStart = System.DateTime.Now;
        //                string appclass = "PermissionReader";
        //                string appfunction = System.Reflection.MethodBase.GetCurrentMethod().Name;

        //                com.StatementCompleted += new StatementCompletedEventHandler(delegate (Object o, StatementCompletedEventArgs a)
        //                {

        //                    DataSource.LogSlowSQL(com, tsStart, appclass, appfunction);
        //                });

        //            }
        //            catch { }
        //            SqlDataAdapter adap = new SqlDataAdapter();
        //            string sql = string.Empty;
        //            try
        //            {

        //                #region Commented by Wendell
        //                //sql = " SELECT  role " ;
        //                //    sql = sql + " FROM userrole a, [user] b, tbluserhascompany c " ;
        //                //sql = sql + "WHERE a.usr *= b.usr ";
        //                //sql = sql + "AND a.usr = '" + UserName + "' ";
        //                //sql = sql + "AND c.usr = a.usr ";
        //                //sql = sql + "AND c.company = '" + LoginCompany + "'";
        //                #endregion

        //                //sql = "SELECT role " +
        //                //    "FROM userrole a, [user] b " +
        //                //    "WHERE a.usr *= b.usr AND a.usr = '" + UserName + "'";

        //                /* Fixed *= operator compatibility issue -- Fu Yu 7/11/2014*/
        //                sql = @"
        //SELECT role 
        //FROM userrole a
        //	LEFT OUTER JOIN [user] b ON a.usr = b.usr
        //WHERE a.usr = @username";


        //                com.Connection = ln.GetSqlConnection();
        //                com.CommandType = CommandType.Text;
        //                com.CommandText = sql;
        //                com.Parameters.AddWithValue("@username", UserName);

        //                adap.SelectCommand = com;
        //                adap.Fill(rDataTable);
        //            }
        //            catch (Exception ex)
        //            {
        //                MessageBox.Show(ex.Message, "RetrieveRole - NetCRM");
        //            }
        //            finally
        //            {
        //                adap.Dispose();
        //                com.Dispose();
        //                ln.Dispose();
        //            }
        //        }

        //        public string GetUserFromRole(string permission_name)
        //        {
        //            string sql = "SELECT DISTINCT a.usr " +
        //                " FROM userrole a " +
        //                " INNER JOIN [user] b ON a.usr = b.usr  AND b.company = '" + LoginCompany.Trim() + "' " +
        //                 " WHERE role = '" + permission_name.Trim() + "'";

        //            return Database.GetValue("NETCRM", sql, "usr");
        //        }



        //        public bool HasRole(string RoleName)
        //        {
        //            bool b = false;

        //            if (rDataTable.Columns["role"] != null)
        //            {
        //                rDataTable.TableName = "role";
        //                DataView dv = new DataView();
        //                dv.Table = rDataTable;
        //                dv.RowFilter = "role = '" + RoleName + "'";
        //                if (dv.Count == 1) b = true;
        //                dv.Dispose();
        //            }
        //            return b;
        //        }

        //        /// <summary>
        //        /// Check the user has the form or reprot access 
        //        /// </summary>
        //        /// <param name="screenName">from or report name </param>
        //        /// <returns></returns>
        //        public bool HasScreen(string screenName)
        //        {
        //            try
        //            {
        //                string sql = string.Format(@"
        //select *
        //from screenmenu 
        //where menu = '{0}'  and menuvalue = 1 
        //		and screen in (
        //						select screen 
        //						from userscreen 
        //						where usr = '{1}')", screenName.Trim(), PermissionReader.LoginName);

        //                return Database.GetDataTable("NETCRM", sql).Rows.Count > 0;
        //            }
        //            catch (Exception ex)
        //            {
        //                MessageBox.Show("An error( " + ex.Message + " ) was encountered while attempting to check if the user has the screen access from the Database [PermissionReader.HasScreen()]");
        //                return false;
        //            }
        //        }

        //        /// <summary>
        //        /// Read Permission from database for a particular user name
        //        /// when user login and keep this in a data table.
        //        /// </summary>
        //        /// <param name="userName"></param>		
        //        public void RetrievePermission(string UserName)
        //        {
        //            //Clear any old entries in the data table (mDataTable)
        //            mDataTable.Clear();

        //            //Retrieve the new values from the database and store them into the data table (mDataTable)
        //            NETCRM ln = new NETCRM();
        //            SqlCommand com = new SqlCommand();
        //            try
        //            {
        //                System.DateTime tsStart = System.DateTime.Now;
        //                string appclass = "PermissionReader";
        //                string appfunction = System.Reflection.MethodBase.GetCurrentMethod().Name;

        //                com.StatementCompleted += new StatementCompletedEventHandler(delegate (Object o, StatementCompletedEventArgs a)
        //                {

        //                    DataSource.LogSlowSQL(com, tsStart, appclass, appfunction);
        //                });

        //            }
        //            catch { }
        //            SqlDataAdapter adap = new SqlDataAdapter();
        //            string sql;

        //            //sql = "SELECT a.permission, b.screen FROM ( " +
        //            //    " 	SELECT DISTINCT up.usr, up.permission FROM  UserPermission up " +
        //            //    " 	WHERE PermissionValue = 1 " +
        //            //    " 	UNION SELECT ur. usr,rp. permission " +
        //            //    " 	FROM userrole ur INNER JOIN rolepermission rp on rp.role= ur.role " +
        //            //    " )  a INNER JOIN [user] b  on a.usr = b.usr INNER JOIN tblUserHasCompany c  on  c.usr = a.usr  " +
        //            //    " WHERE a.usr = '" + UserName + "' " +
        //            //    " AND c.company = '" + LoginCompany + "'";

        //            #region Commented by Wendell
        //            //sql = "SELECT a.permission, b.screen ";
        //            //sql = sql + "FROM UserPermission a, [user] b, tblUserHasCompany c ";
        //            //sql = sql + "WHERE a.usr *= b.usr ";
        //            //sql = sql + "AND a.usr = '" + UserName + "' ";
        //            //sql = sql + "AND c.usr = a.usr ";
        //            //sql = sql + "AND a.PermissionValue = 1 ";
        //            //sql = sql + "AND c.company = '" + LoginCompany + "'";
        //            #endregion
        //            //sql = "SELECT a.permission, b.screen " +
        //            //    "FROM UserPermission a, [user] b " +
        //            //    "WHERE a.usr *= b.usr AND a.usr = '" + UserName + "' " +
        //            //    "AND a.PermissionValue = 1";

        //            /* Fixed *= operator compatibility issue -- Fu Yu 7/11/2014*/
        //            sql = @"
        //SELECT a.permission, b.screen, b.name, b.office
        //FROM UserPermission a
        //	LEFT OUTER JOIN [user] b ON a.usr = b.usr 
        //WHERE a.usr = @username AND a.PermissionValue = 1 ";

        //            com.Connection = ln.GetSqlConnection();
        //            com.CommandType = CommandType.Text;
        //            com.CommandText = sql;
        //            com.Parameters.AddWithValue("@username", UserName);

        //            adap.SelectCommand = com;
        //            adap.Fill(mDataTable);

        //            adap.Dispose();
        //            com.Dispose();
        //            ln.Dispose();
        //            mLoginName = UserName;

        //            if (mDataTable.Rows.Count > 0)
        //            {
        //                mScreen = mDataTable.Rows[0]["screen"].ToString().Trim();
        //                mName = mDataTable.Rows[0]["name"].ToString().Trim();
        //                mOffice = mDataTable.Rows[0]["office"].ToString().Trim();
        //            }
        //            else
        //                mScreen = string.Empty;
        //            adap.Dispose();
        //            com.Dispose();
        //            ln.Dispose();
        //            if (mScreen.Trim() == string.Empty)
        //            {
        //                sql = ("SELECT DISTINCT * from [user] " +
        //                    "WHERE usr = '" + UserName + "'");
        //                mScreen = Database.GetValue("NETCRM", sql.Trim(), "screen").Trim();
        //            }
        //        }
        //        /// <summary>
        //        /// Returns the user that has permission from a given permission name.
        //        /// </summary>
        //        public string GetUserFromPermission(string permission_name)
        //        {
        //            /*string sql = "SELECT DISTINCT a.usr " + 
        //                "FROM userpermission a " + 
        //                "INNER JOIN [user] b ON a.usr = b.usr " + 
        //                "WHERE a.usr IN " +
        //                "(SELECT usr " + 
        //                "FROM userpermission " +  
        //                "WHERE permission = '" + permission_name.Trim() + "' " + 
        //                "AND permissionvalue = 1)" + 
        //                "AND b.company = '" + LoginCompany.Trim() + "'";*/
        //            string sql = "SELECT DISTINCT a.usr " +
        //                " FROM userpermission a " +
        //                " INNER JOIN [user] b ON a.usr = b.usr  AND b.company = '" + LoginCompany.Trim() + "' " +
        //                 " WHERE permission = '" + permission_name.Trim() + "'  AND a.permissionvalue = 1";

        //            return Database.GetValue("NETCRM", sql, "usr");
        //        }

        //        public string GetUserFromCompany(string Permission)
        //        {
        //            //bool a = false;
        //            string sql = "select a.[usr] " +
        //                    "from userpermission a, [user] b " +
        //                    "where a.usr = b.usr " +
        //                    "and a.permission = '" + Permission.Trim() + "' " +
        //                    "and a.permissionvalue = 1 " +
        //                    "and b.company = '" + LoginCompany.Trim() + "' " +
        //                    "and a.usr in (select usr " +
        //                    "from userpermission " +
        //                    "where permission = '" + Permission.Trim() + "' " +
        //                    //"and usr = '" + LoginName.Trim() + 
        //                    " )";
        //            return Database.GetValue("NETCRM", sql, "usr");

        //        }
        //        /// <summary>
        //        /// Return yes if PermissionName is exist in mDataTable.
        //        /// </summary>
        //        /// <param name="s"></param>
        //        /// <returns></returns>
        //        public bool HasPermission(string PermissionName)
        //        {
        //            bool b = false;

        //            if (mDataTable.Columns["permission"] != null)
        //            {
        //                mDataTable.TableName = "permission";
        //                DataView dv = new DataView();
        //                dv.Table = mDataTable;
        //                dv.RowFilter = "Permission = '" + PermissionName + "'";
        //                if (dv.Count == 1) b = true;
        //                dv.Dispose();

        //                //string YesOrNo;
        //                //if (b) YesOrNo = "Y";
        //                //else YesOrNo = "N";
        //                //NETCRM ln = new NETCRM();
        //                //ln.Execute("insert into AppLogs(usr, permission, [time],valid) values('"+mLoginName+"','"+PermissionName+"',getdate(),'"+YesOrNo+"')");
        //                //ln.Dispose();
        //            }

        //            return b;
        //        }



        //        /// <summary> This internal function checks to see if the Labs Data has been loaded from the database. If the data is not loaded, this function automatically retrieves the labs settings for the current user from the database. Unlike the other (cough) functions in this class, it is not required to be called manually for example when logging in and thus should stay PRIVATE. This function is automatically called within this class when it is required. </summary>
        //        private void EnsureLabsAreLoaded()
        //        {
        //            try
        //            {
        //                //Check if Labs are already Loaded
        //                if (mLabFeatures == null)
        //                {

        //                    //Initialise
        //                    mLabFeatures = new List<string>(3);

        //                    //Setup Query
        //                    string SQL = @"
        //                        select * from tblLabsEnabled
        //                        where usr = '" + PermissionReader.LoginName + @"'
        //                    ";

        //                    //Retrieve Labs Settings from the Database for the User 
        //                    DataTable dt = Database.GetDataTable("NETCRM", SQL);

        //                    //Check that Results were Retrieved
        //                    if (dt.Rows.Count > 0)
        //                    {
        //                        //Loop through the results
        //                        foreach (DataRow row in dt.Rows)
        //                        {
        //                            //Check if the specified Lab is Enabled
        //                            if (row["Enabled"].ToString() == "True")
        //                            {
        //                                //Store the Lab Feature in the Enabled Labs List
        //                                mLabFeatures.Add(row["Feature"].ToString());
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        //No Labs Data retrieved from the database
        //                    }

        //                }
        //                else
        //                {
        //                    //Labs have previously been loaded (no need to reload)
        //                }
        //            }
        //            catch
        //            {
        //                //Alert the user an error was encountered
        //                MessageBox.Show("An error was encountered while attempting to load Labs Features from the Database [PermissionReader.RetrieveLabs()]");

        //                //Use no Labs for this session
        //                mLabFeatures = new List<string>(3);
        //            }
        //        }

        //        /// <summary> Labs are designed so that they can be quickly and easily disabled by the programmer if there is a problem with the function. This function indicates whether the specified lab has been globally disabled. </summary>
        //        /// <param name="labFeature">A string representing the lab feature (it must be spelt correctly)</param>
        //        public bool IsLabGloballyDisabled(string labFeature)
        //        {
        //            try
        //            {
        //                //Setup Query
        //                string SQL = @"
        //                    select top 1 Enabled
        //                    from tblLabs
        //                    where Feature = '" + labFeature + @"'
        //                    or Feature = 'Labs'
        //                    order by Enabled asc
        //                ";

        //                //Retrieve Labs Settings from the Database for the User 
        //                DataTable dt = Database.GetDataTable("NETCRM", SQL);

        //                //Check that Results were Retrieved
        //                if (dt.Rows.Count > 0)
        //                {
        //                    //Check if the specified Lab is Enabled
        //                    if (dt.Rows[0]["Enabled"].ToString() == "True")
        //                    {
        //                        //Lab is not Disabled
        //                        return false;
        //                    }
        //                    else
        //                    {
        //                        //Lab IS Disabled
        //                        return true;
        //                    }
        //                }
        //                else
        //                {
        //                    //No results Retrieved. Assume lab is disabled
        //                    return true;
        //                }
        //            }
        //            catch
        //            {
        //                //Alert the user an error was encountered
        //                MessageBox.Show("An error was encountered while attempting to load Labs Features from the Database [PermissionReader.RetrieveLabs()]");

        //                //Use no Labs for this session
        //                mLabFeatures = new List<string>(3);

        //                //Report the lab as globally disabled
        //                return true;
        //            }
        //        }

        //        /// <summary> Returns a boolean value indicating whether a specific Lab feature has been enabled or disabled by the user. </summary>
        //        /// <param name="labFeature">A string representing the lab feature (it must be spelt correctly)</param>
        //        public bool HasLabEnabled(string labFeature)
        //        {
        //            //Ensure Labs are already Loaded
        //            EnsureLabsAreLoaded();

        //            //Check to see if Lab is Disabled
        //            if (IsLabGloballyDisabled(labFeature))
        //            {
        //                //Temporarily Disable Lab Feature (since it has been globally disabled by the programmer)
        //                //DisableLab(labFeature, /*Don't update the database*/false);

        //                //Report the lab as disabled
        //                return false;
        //            }

        //            //Check to see if the list of enabled Lab Features contains the requested feature
        //            if (mLabFeatures.Contains(labFeature))
        //            {
        //                //Lab Feature Enabled, return true
        //                return true;
        //            }
        //            else
        //            {
        //                //Lab Feature Not Enabled, return false
        //                return false;
        //            }

        //        }



        //        /// <summary> Enables a specific labs feature </summary>
        //        /// <param name="labFeature">A string representing the lab feature to enable. It must be spelt correctly</param>
        //        public void EnableLab(string labFeature)
        //        {
        //            //Pass through function below 
        //            EnableLab(labFeature, true);
        //        }
        //        /// <summary> Enables a specific labs feature, optionally specifying whether the setting should be saved to the database (TRUE) or simply applied to this sessin only (FALSE) </summary>
        //        /// <param name="labFeature">A string representing the lab feature to enable. It must be spelt correctly</param>
        //        /// <param name="thisSessionOnly">A boolean value indicating whether the database should be updated</param>
        //        public void EnableLab(string labFeature, bool updateDatabase)
        //        {
        //            //Ensure Labs are already Loaded
        //            EnsureLabsAreLoaded();

        //            //Check that the Lab Feature has not already been enabled
        //            if (mLabFeatures.Contains(labFeature) == false)
        //            {
        //                //Add the specified lab feature to the enabled labs list
        //                mLabFeatures.Add(labFeature);

        //                //Report the labs feature change to other forms
        //                if (LabsChanged != null) LabsChanged(labFeature, true);
        //            }

        //            //Check if we should update the database
        //            if (updateDatabase == true)
        //            {
        //                //Update the Database
        //                string SQL = @"
        //                    -- Enable Lab
        //                    if not exists (select * from tblLabsEnabled where usr = '" + PermissionReader.LoginName + @"' and Feature = '" + labFeature + @"')
        //	                    insert into tblLabsEnabled(usr, Feature, Enabled)
        //	                    values ('" + PermissionReader.LoginName + @"', '" + labFeature + @"', 1)
        //                    else
        //	                    select 'No action required'

        //                    -- Update History
        //                    insert into tblLabsHistory (usr, Feature, Enabled, Description, TimeStamp)
        //                    values ('" + PermissionReader.LoginName + @"', '" + labFeature + @"', 1, 'Lab Enabled by User', getdate())
        //                ";
        //                Database.ExecuteSQL(/*ClsConfig.NETCRMDB*/"NetCRM", SQL);
        //            }
        //        }

        //        /// <summary> Disables a specific labs feature </summary>
        //        /// <param name="labFeature">A string representing the lab feature to disable. It must be spelt correctly</param>
        //        public void DisableLab(string labFeature)
        //        {
        //            //Pass through function below 
        //            DisableLab(labFeature, true);
        //        }
        //        /// <summary> Disables a specific labs feature, optionally specifying whether the setting should be saved to the database (TRUE) or simply applied to this sessin only (FALSE) </summary>
        //        /// <param name="labFeature">A string representing the lab feature to disable. It must be spelt correctly</param>
        //        public void DisableLab(string labFeature, bool updateDatabase)
        //        {
        //            //Ensure Labs are already Loaded
        //            EnsureLabsAreLoaded();

        //            //Check if the Lab Feature is already enabled
        //            if (mLabFeatures.Contains(labFeature))
        //            {
        //                //Remove the specified lab feature from the list
        //                mLabFeatures.Remove(labFeature);

        //                //Report the labs feature change to other forms
        //                if (LabsChanged != null) LabsChanged(labFeature, false);
        //            }

        //            //Check if we should update the database
        //            if (updateDatabase == true)
        //            {
        //                //Update the Database
        //                string SQL = @"
        //                    -- Disable Lab
        //                    if exists (select * from tblLabsEnabled where usr = '" + PermissionReader.LoginName + @"' and Feature = '" + labFeature + @"')
        //	                    delete from tblLabsEnabled
        //	                    where usr = '" + PermissionReader.LoginName + @"' and Feature = '" + labFeature + @"'
        //                    else
        //	                    select 'No action required'

        //                    -- Update History
        //                    insert into tblLabsHistory (usr, Feature, Enabled, Description, TimeStamp)
        //                    values ('" + PermissionReader.LoginName + @"', '" + labFeature + @"', 0, 'Lab Disabled by User', getdate())
        //                ";
        //                Database.ExecuteSQL(/*ClsConfig.NETCRMDB*/"NetCRM", SQL);
        //            }
        //        }

        //        public bool HasAffinity(string Affinity)
        //        {
        //            bool b = false;

        //            //DataTable dt = new DataTable();
        //            //string SQL = "SELECT * FROM tblUserDepartment a " +
        //            //    "INNER JOIN DepartmentCusType b ON a.Department = b.DepartmentName " +
        //            //    "WHERE a.usr = '" + LoginName + "' AND b.CusTypeCD = '" + Affinity + "'";

        //            //dt = Database.GetDataTable("NETCRM", SQL);
        //            //if (dt.Rows.Count > 0)
        //            //    b = true;

        //            DataTable dt = Database.GetDataTable("NETCRM", string.Format("EXEC spHasAffinity '{0}', '{1}' ", LoginName, Affinity));

        //            if (dt.Rows.Count > 0)
        //                b = true;

        //            return b;
        //        }

        //        /// <summary>
        //        /// Checks to see if the user is the sales rep for the given customer.
        //        /// </summary>
        //        /// <param name="macCusCode"></param>
        //        /// <returns></returns>
        //        public bool HasRepAccess(string macCusCode)
        //        {
        //            bool result = false;

        //            string sql = @"
        //                SELECT a.Slspsn_No
        //                FROM CusSlspsn a 
        //	                INNER JOIN ARCUSFIL_SQL b ON a.Slspsn_No = b.slspsn_no
        //                WHERE a.usr = '{0}'
        //	                AND b.cus_no = '{1}'";

        //            DataTable dt = Database.GetDataTable("NETCRM", string.Format(sql, PermissionReader.LoginName, macCusCode));

        //            if (dt.Rows.Count > 0)
        //                result = true;

        //            return result;
        //        }

        //        //added by Malin to record user's computer name and login time and logout time. 
        //        public void RecordUserSessionID()
        //        {
        //            //bool a = false;
        //            string sql = "select top 1 id " +
        //                "from DebugLoginActivity " +
        //                "where  loginName='" + PermissionReader.LoginName + "' " +
        //                "Order By id desc ";
        //            mgblLSessionID = Database.GetValue("NETCRM", sql, "id");

        //        }

        //        public string GetEmailByUser(string user)
        //        {
        //            return Database.GetValue("NETCRM", "select * from [user] where usr = '" + user + "'", "email");
        //        }

        //        public string GetUserSessionID()
        //        {
        //            //bool a = false;
        //            return (mgblLSessionID);

        //        }

        //        public bool CanViewOrders(string strValue, string tmpVal, string form, bool isBool)
        //        {
        //            bool isValid = false;
        //            DataTable dtValid = new DataTable();
        //            DataTable dTable = new DataTable();

        //            if (form == "cus")
        //            {
        //                #region commented
        //                /*string sql = "SELECT a.usr, a.affinity FROM tblUserAffinity a " +
        //                    "INNER JOIN cisect b ON a.affinity = b.sct_code " +
        //                    "INNER JOIN arcusfil_sql c ON a.affinity = c.cus_type_cd " +
        //                    "WHERE exact_no='" + strValue + "'" +
        //                    "AND a.usr ='" + LoginName + "'";*/

        //                #endregion
        //                string sql = "SELECT DISTINCT a.usr, b.CusTypeCd FROM tblUserDepartment a " +
        //                    "INNER JOIN DepartmentCusType b ON b.DepartmentName = a.department " +
        //                    "INNER JOIN arcusfil_sql c ON c.cus_type_cd = b.CusTypeCd " +
        //                    "INNER JOIN oeordhdr_sql d ON LTRIM(RTRIM(d.cus_no)) = c.exact_no " +
        //                    "INNER JOIN oeordlin_sql e ON e.ord_no = d.ord_no " +
        //                    "WHERE c.exact_no = '" + strValue + "' " +
        //                    "AND d.status <> 'H' ";

        //                if (isBool)
        //                    sql = sql + "AND ((SELECT SUM(esr_aantal) FROM " + mDummySetting + ".liv_vu_orsrg WHERE ordernr = d.ExactOrderNo AND artcode = e.item_no) <> " +
        //                        "(SELECT SUM(aant_gelev) FROM " + mDummySetting + ".liv_vu_orsrg WHERE ordernr = d.ExactOrderNo AND artcode = e.item_no)) ";

        //                sql = sql + "AND  a.usr = '" + LoginName + "'";
        //                dtValid = Database.GetDataTable("NETCRM", sql);
        //                mDummySetting = string.Empty;
        //            }
        //            else if (form == "sales")
        //            {
        //                #region commented
        //                /*string sql = "SELECT Distinct a.usr, a.affinity FROM tblUserAffinity a " +
        //                "INNER JOIN cisect b ON a.affinity = b.sct_code " +
        //                "INNER JOIN arcusfil_sql c ON a.affinity = c.cus_type_cd " +
        //                "INNER JOIN cusslspsn d ON c.slspsn_no = d.slspsn_no " +
        //                "WHERE d.slspsn_no ='" +strValue + "'" +
        //                "AND a.usr ='" + LoginName + "'";*/
        //                #endregion
        //                string sql = "SELECT DISTINCT a.usr, b.CusTypeCd FROM tblUserDepartment a " +
        //                    "INNER JOIN DepartmentCusType b ON b.DepartmentName = a.department " +
        //                    "INNER JOIN arcusfil_sql c ON c.cus_type_cd = b.CusTypeCd " +
        //                    "INNER JOIN CusSlsPsn d ON d.slspsn_no = c.slspsn_no " +
        //                    "INNER JOIN oeordhdr_sql e ON e.cus_no = c.exact_no " +
        //                    "INNER JOIN oeordlin_sql f ON f.ord_no = e.ord_no " +
        //                    "LEFT OUTER JOIN btwtrs g ON g.btwtrans = f.tax_sched " +
        //                    "INNER JOIN " + mDummySetting + ".liv_vu_orsrg vlin ON e.ExactOrderNo = vlin.ordernr  " +
        //                    "WHERE d.slspsn_no = '" + strValue + "' " +
        //                    "AND a.usr = '" + LoginName + "' " +
        //                    " AND e.OrderStatus != 'H' AND e.exactorderno IS NOT NULL";

        //                dtValid = Database.GetDataTable("NETCRM", sql);
        //                mDummySetting = string.Empty;
        //            }
        //            else if (form == "ordInv")
        //            {
        //                #region commented
        //                /*string sql = "SELECT a.usr, a.affinity FROM tblUserAffinity a " +
        //                         "INNER JOIN cisect b ON a.affinity = b.sct_code " +
        //                         "INNER JOIN arcusfil_sql c ON a.affinity = c.cus_type_cd " +
        //                         "INNER JOIN oeordhdr_sql d ON c.cus_no = d.cus_no " +
        //                         "WHERE d.ord_no ='" + strValue + "' " +
        //                         "AND a.usr='" + LoginName + "'";
        //                string sql2 = "SELECT a.usr, a.affinity FROM tblUserAffinity a " +
        //                          "INNER JOIN cisect b ON a.affinity = b.sct_code " +
        //                          "INNER JOIN arcusfil_sql c ON a.affinity = c.cus_type_cd " +
        //                          "INNER JOIN frhkrg d ON c.debnr = d.debnr " +
        //                          "WHERE d.faknr ='" + strValue + "' " +
        //                          "AND a.usr='" + LoginName + "'";*/
        //                #endregion
        //                string sql = "SELECT DISTINCT a.usr, b.CusTypeCd FROM tblUserDepartment a " +
        //                    "INNER JOIN DepartmentCusType b ON b.DepartmentName = a.department " +
        //                    "INNER JOIN arcusfil_sql c ON c.cus_type_cd = b.CusTypeCd " +
        //                    "INNER JOIN oeordhdr_sql d ON d.cus_no = c.Exact_no " +
        //                    "LEFT OUTER JOIN frhkrg e ON d.ExactOrderNo = e.ordernr " +
        //                    "WHERE d.ExactOrderNo is not null AND d.ExactOrderNo = '" + strValue + "' " +
        //                    "AND a.usr = '" + LoginName + "' ";
        //                string sql2 = "SELECT DISTINCT a.usr, b.CusTypeCd FROM tblUserDepartment a " +
        //                    "INNER JOIN DepartmentCusType b ON b.DepartmentName = a.department " +
        //                    "INNER JOIN arcusfil_sql c ON c.cus_type_cd = b.CusTypeCd " +
        //                    "INNER JOIN oeordhdr_sql e ON e.cus_no = c.exact_no " +
        //                    "INNER JOIN frhkrg d ON d.ordernr = e.ExactOrderNo " +
        //                    "WHERE d.faknr = " + strValue + " " +
        //                    "AND a.usr = '" + LoginName + "' ";

        //                dtValid = isBool == true ? Database.GetDataTable("NETCRM", sql) : Database.GetDataTable("NETCRM", sql2);
        //            }
        //            else if (form == "CurrentSO_item")
        //            {
        //                string sql = "EXEC dbo.spProductCurrentSO " +
        //                    (strValue == "" ? "null" : "'" + strValue + "'") + ",null," +
        //                    (tmpVal == "" ? "null" : "'" + tmpVal + "'") +
        //                    ",null,null,0,0,null,null,null,'" + LoginName + "',1";

        //                dtValid = Database.GetDataTable("NETCRM", sql);
        //            }
        //            else if (form == "CurrentSO_sales")
        //            {
        //                string sql = "EXEC dbo.spProductCurrentSO null,null,null,null,null,0,0,null," +
        //                    (strValue == "" ? "null" : "'" + strValue + "'") + ",null,'" + LoginName + "',1";

        //                dtValid = Database.GetDataTable("NETCRM", sql);
        //            }
        //            else if (form == "CurrentSO_ordInv")
        //            {
        //                string sql = "dbo.spProductCurrentSO null,null,null,null,null," + (isBool ? "1" : "0") + "," + (isBool ? "0" : "1") + "," +
        //                    (strValue == "" ? "null" : "'" + strValue + "'") + ",null,null,'" + LoginName + "',1";

        //                dtValid = Database.GetDataTable("NETCRM", sql);
        //            }
        //            else if (form == "HistorySO_item")
        //            {
        //                string sql = "EXEC dbo.spProductSOHistory " +
        //                    (strValue == "" ? "null" : "'" + strValue + "'") + ",null," +
        //                    (tmpVal == "" ? "null" : "'" + tmpVal + "'") +
        //                    ",null,null,0,0,null,'" + LoginName + "',1,1, " +
        //                    (!(PermissionReader.HasPermission("WHview")) ? "1" : "0") + " ";

        //                dtValid = Database.GetDataTable("NETCRM", sql);
        //            }
        //            else if (form == "HistorySO_ordInv")
        //            {
        //                string sql = "EXEC dbo.spProductSOHistory null,null,null,null,null," + (isBool ? "1" : "0") +
        //                    "," + (isBool ? "0" : "1") + "," + (strValue == "" ? "null" : "'" + strValue + "'") + ",'" + LoginName + "',1,1, " +
        //                    (!(PermissionReader.HasPermission("WHview")) ? "1" : "0") + " ";

        //                dtValid = Database.GetDataTable("NETCRM", sql);
        //            }
        //            else if (form == "NetCRM_item")
        //            {
        //                #region commented
        //                //string sql = "SELECT Distinct a.usr, a.affinity FROM tblUserAffinity a " +
        //                //"INNER JOIN cisect b ON a.affinity = b.sct_code " +
        //                //"INNER JOIN arcusfil_sql c ON a.affinity = c.cus_type_cd " +
        //                //"INNER JOIN oeordhdr_sql d ON c.cus_no = d.cus_no " +
        //                //"INNER JOIN oeordlin_sql e ON d.ord_no = e.ord_no " +
        //                //"INNER JOIN Imitmidx_sql f ON e.item_no = f.item_no " +
        //                //"WHERE f.item_no='" + strValue + "' " +
        //                //"AND a.usr ='" + LoginName + "'";
        //                #endregion
        //                string sql = "SELECT DISTINCT a.usr, b.CustypeCd " +
        //                    "FROM tblUserDepartment a " +
        //                    "INNER JOIN DepartmentCusType b ON b.DepartmentName = a.department " +
        //                    "INNER JOIN arcusfil_sql c ON c.cus_type_cd = b.CusTypecd " +
        //                    "INNER JOIN oeordhdr_sql d ON d.cus_no = c.exact_no " +
        //                    "INNER JOIN oeordlin_sql e ON e.ord_no = d.ord_no " +
        //                    "AND d.ExactOrderNo IS NOT NULL AND d.OrderStatus <> 'H' " +
        //                    "WHERE a.usr ='" + LoginName + "' " +
        //                    "AND e.qty_ordered <> ISNULL((SELECT sum(aant_gelev) FROM " + mDummySetting +
        //                    ".liv_vu_orsrg WHERE Ordernr = d.ExactOrderNo AND artcode = e.item_no AND reeds_fakt = 1),0) ";

        //                if (!(PermissionReader.HasPermission("WHview")))
        //                    sql += " AND d.mfg_loc in ('R','T','V','PM') ";

        //                sql += (strValue == string.Empty ? "" : " AND e.item_no = '" + strValue + "' ");
        //                sql += (tmpVal == string.Empty ? "" : " AND c.exact_no = '" + tmpVal + "' ");

        //                dtValid = Database.GetDataTable("NETCRM", sql);
        //                DummySetting = string.Empty; // needed to set to default value
        //            }
        //            else if (form == "Exact_item")
        //            {
        //                string sql = "SELECT DISTINCT a.usr, b.CustypeCd " +
        //                    "FROM " + mDummySetting + ".tblUserDepartment a " +
        //                    "INNER JOIN " + mDummySetting + ".DepartmentCusType b ON b.DepartmentName = a.department " +
        //                    "INNER JOIN liv_vu_cicmpy c ON c.sct_code = b.CusTypecd AND c.cmp_type = 'C' " +
        //                    "INNER JOIN liv_vu_orkrg d ON d.debnr = c.debnr " +
        //                    "INNER JOIN liv_vu_orsrg e ON e.ordernr = d.ordernr " +
        //                    "INNER JOIN liv_vu_humres f ON f.res_id = d.represent_id " +
        //                    "WHERE a.usr = '" + LoginName + "' " +
        //                    "AND e.ar_soort = 'V' AND d.status = 'V' " +
        //                    "AND e.esr_aantal <> ISNULL((SELECT sum(aant_gelev) FROM liv_vu_orsrg WHERE Ordernr = d.ordernr AND artcode = e.artcode AND reeds_fakt = 1),0) ";

        //                if (!(PermissionReader.HasPermission("WHview")))
        //                    sql += " AND d.magcode in ('R','T','V','PM') ";

        //                sql += (strValue == string.Empty ? "" : " AND e.artcode = '" + strValue + "' ");
        //                sql += (tmpVal == string.Empty ? "" : " AND ltrim(rtrim(c.cmp_code)) = '" + tmpVal + "' ");

        //                dtValid = Database.GetDataTable("LivExact", sql);
        //                DummySetting = string.Empty; // needed to set to default value
        //            }
        //            else if (form == "hstItem")
        //            {
        //                string sql = "SELECT DISTINCT a.usr, b.CusTypeCd FROM tblUserDepartment a " +
        //                    "INNER JOIN DepartmentCusType b ON b.DepartmentName = a.department " +
        //                    "INNER JOIN arcusfil_sql c ON c.cus_type_cd = b.CusTypeCd " +
        //                    "INNER JOIN frhkrg d ON d.debnr = c.debnr " +
        //                    "INNER JOIN frhsrg e ON e.ordernr = d.ordernr " +
        //                    "INNER JOIN orhsrg f ON f.ordernr = d.ordernr " +
        //                    "INNER join orhkrg g on g.ordernr = f.ordernr " +
        //                    "WHERE a.usr = '" + LoginName + "' " +
        //                    "AND g.ord_soort <> 'q' AND e.ordernr IS NOT NULL";

        //                sql += (strValue == string.Empty ? "" : " AND LTRIM(RTRIM(c.exact_no)) ='" + strValue + "' ");
        //                sql += (tmpVal == string.Empty ? "" : " AND LTRIM(RTRIM(e.artcode)) ='" + tmpVal + "' ");

        //                //sql += isBool == true ? "LTRIM(RTRIM(b.exact_no)) ='" + strValue + "'" : "LTRIM(RTRIM(d.artcode)) ='" + strValue + "'";

        //                dtValid = Database.GetDataTable("NETCRM", sql);
        //            }
        //            else if (form == "hstCus")
        //            {
        //                #region commented
        //                /*string sql = "SELECT DISTINCT a.usr, a.affinity FROM tblUserAffinity a " +
        //                             "INNER JOIN cisect b ON a.affinity = b.sct_code " +
        //                             "INNER JOIN arcusfil_sql c ON a.affinity = c.cus_type_cd " +
        //                             "INNER JOIN frhkrg d ON c.debnr = d.debnr " +
        //                             "WHERE exact_no ='" + strValue + "' " +
        //                             "AND a.usr ='" + LoginName + "'";*/
        //                #endregion
        //                string sql = "EXEC ar_OrderByCusPONo '" + strValue + "','','" + LoginName + "',1 ";
        //                dtValid = Database.GetDataTable("NETCRM", sql);
        //            }
        //            else if (form == "hstOrdInv")
        //            {
        //                #region commented
        //                //string sql = "select distinct a.usr, a.affinity from tbluseraffinity a " +
        //                //             "inner join arcusfil_sql b on a.affinity = b.cus_type_cd " +
        //                //             "join cisect c on b.cus_type_cd = c.sct_code " +
        //                //             "join oeordhdr_sql d on b.exact_no = d.cus_no " +
        //                //             "join frhkrg e on d.exactOrderNo = e.ordernr " +
        //                //             "WHERE ";
        //                //string sql = "SELECT DISTINCT a.usr, b.CusTypeCd FROM tblUserDepartment a " +
        //                //             "INNER JOIN DepartmentCusType b ON a.Department = b.DepartmentName " +
        //                //             "INNER JOIN arcusfil_sql c ON b.CusTypeCd = c.cus_type_cd " +
        //                //             "INNER JOIN oeordhdr_sql d ON c.cus_no = d.cus_no " +
        //                //             "WHERE d.ord_no = '" + strValue + "' " +
        //                //             "AND a.usr = '" + LoginName + "'";

        //                //string sql2 = "SELECT DISTINCT a.usr, b.CusTypeCd FROM tblUserDepartment a " +
        //                //              "INNER JOIN DepartmentCusType b ON a.department = b.DepartmentName " +
        //                //              "INNER JOIN arcusfil_sql c ON b.CusTypeCd = c.cus_type_cd " +
        //                //              "INNER JOIN frhkrg d ON c.debnr = d.debnr " +
        //                //              "WHERE d.faknr ='" + strValue + "' " +
        //                //              "AND a.usr = '" + LoginName + "' ";
        //                #endregion

        //                string sql = "SELECT DISTINCT a.usr, b.CustypeCd FROM tblUserDepartment a " +
        //                             "INNER JOIN DepartmentCusType b ON b.departmentname = a.department " +
        //                             "INNER JOIN arcusfil_sql c ON  c.cus_type_cd = b.CusTypeCD " +
        //                             "INNER JOIN frhkrg d ON d.Debnr = c.debnr " +
        //                             "WHERE ";

        //                sql += isBool == true ? "d.ordernr = " + strValue + " AND a.usr = '" + LoginName + "'" :
        //                                        "d.faknr = " + strValue + " AND a.usr = '" + LoginName + "'";
        //                //sql += isBool == true ? "d.ExactOrderNo='" + strValue + "' " : "e.faknr='" + strValue + "'";
        //                dtValid = Database.GetDataTable("NETCRM", sql);

        //            }
        //            else if (form == "itmSoldq")
        //            {
        //                #region commented
        //                //string sql = "select distinct a.usr, a.affinity from tbluseraffinity a " +
        //                //             "inner join arcusfil_sql b on a.affinity = b.cus_type_cd " +
        //                //             "join cisect c on c.sct_code = b.cus_type_cd " +
        //                //             "join frhkrg d on d.debnr = b.debnr " +
        //                //             "join frhsrg e on d.ordernr = e.ordernr " +
        //                //             "join imitmidx_sql f on f.item_no = e.artcode " +
        //                //             "WHERE exact_no ='" + strValue + "'" +
        //                //             "AND a.usr ='" + LoginName + "'";
        //                #endregion
        //                string fromdt = string.Empty, todt = string.Empty;
        //                string sql = "select distinct a.usr, b.custypecd " +
        //                    "from tbluserdepartment a " +
        //                    "inner join departmentcustype b on b.departmentname = a.department " +
        //                    "inner join arcusfil_sql c on c.cus_type_cd = b.custypecd " +
        //                    "inner join frhkrg d on d.debnr = c.debnr " +
        //                    "inner join frhsrg e on e.ordernr = d.ordernr " +
        //                    "inner join imitmidx_sql f on f.item_no = e.artcode " +
        //                    "WHERE c.exact_no ='" + strValue + "' AND " +
        //                    "a.usr ='" + LoginName + "' " +
        //                    "AND e.ar_soort = 'V' ";

        //                if (tmpVal != string.Empty)
        //                {
        //                    fromdt = tmpVal.Split(";".ToCharArray())[0];
        //                    todt = tmpVal.Split(";".ToCharArray())[1];
        //                    sql = sql + "AND d.fakdat >= '" + fromdt + "' AND d.fakdat <= '" + todt + "' ";
        //                }

        //                dtValid = Database.GetDataTable("NETCRM", sql);
        //            }
        //            else if (form == "OrdHstItmCus 08-31-2009")
        //            {
        //                string sql = "SELECT DISTINCT a.usr, b.CustypeCd FROM tblUserDepartment a " +
        //                    "INNER JOIN DepartmentCusType b ON b.DepartmentName = a.department " +
        //                    "INNER JOIN arcusfil_sql c ON c.cus_type_cd = b.CusTypecd " +
        //                    "INNER JOIN oehdrhst_sql d ON d.cus_no = c.cus_no " +
        //                    "INNER JOIN oelinhst_sql e ON e.ord_no = d.ord_no " +
        //                    "WHERE a.usr ='" + LoginName + "'";

        //                sql += (strValue == string.Empty ? "" : " AND e.item_no = '" + strValue + "' ");
        //                sql += (tmpVal == string.Empty ? "" : " AND c.exact_no = '" + tmpVal + "' ");

        //                dtValid = Database.GetDataTable("NETCRM", sql);
        //            }

        //            if (dtValid.Rows.Count > 0)
        //                isValid = true;

        //            return isValid;

        //        }

        //        /// <summary>
        //        /// Get/set Screen Menu.
        //        /// </summary>
        //        public string Screen
        //        {
        //            get { return mScreen; }
        //            set { mScreen = value; }
        //        }

        //        /// <summary>
        //        /// Get or set Use Master status.
        //        /// </summary>
        //        public bool IsUseMaster { get; set; }

        //        /// <summary>
        //        /// Get or set LoginName
        //        /// </summary>
        //        public string LoginName
        //        {
        //            get { return mLoginName; }
        //            set { mLoginName = value; }
        //        }
        //        /// <summary>
        //        /// Get or set Name
        //        /// </summary>
        //        public string Name
        //        {
        //            get { return mName; }
        //            set { mName = value; }
        //        }

        //        /// <summary>
        //        /// Get or set Office
        //        /// </summary>
        //        public string Office
        //        {
        //            get { return mOffice; }
        //            set { mOffice = value; }
        //        }
        //        /// <summary>
        //        /// Get or set LoginCompany
        //        /// </summary>
        //        public string LoginCompany
        //        {
        //            get { return mLoginCompany; }
        //            set { mLoginCompany = value; }
        //        }

        //        /// <summary>
        //        /// Get or set LoginServer
        //        /// </summary>
        //        public string LoginServer
        //        {
        //            get { return mLoginServer; }
        //            set { mLoginServer = value; }
        //        }

        //        /// <summary>
        //        /// Get or set LoginPassword
        //        /// </summary>
        //        public string LoginPassword
        //        {
        //            get { return mLoginPassword; }
        //            set { mLoginPassword = value; }
        //        }

        //        /// <summary>
        //        /// Get or set Active Directory Username
        //        /// </summary>
        //        public string ADUser
        //        {
        //            get { return PermissionReader.mADUsr; }
        //            set { PermissionReader.mADUsr = value; }
        //        }

        //        // global session ID for user till they logoff - added by Malin

        //        public string gblLSessionID
        //        {
        //            get { return mgblLSessionID; }
        //            set { mgblLSessionID = value; }
        //        }

        //        /// <summary>
        //        /// Get or set Description
        //        /// </summary>
        //        public string Description
        //        {
        //            get { return GetDescription(); }
        //            set { mDescription = value; }
        //        }

        //        /// <summary>
        //        /// Retrieves the description for the logged in user. (This is normally their department.)
        //        /// </summary>
        //        private string GetDescription()
        //        {
        //            if (mDescription == null)
        //            {
        //                string sql = "SELECT description FROM [user] WHERE usr ='" + mLoginName + "'";
        //                mDescription = Database.GetValue("NETCRM", sql, "description");
        //            }

        //            return mDescription;
        //        }

        //        internal struct LASTINPUTINFO
        //        {
        //            public uint cbSize;
        //            public uint dwTime;
        //        }

        //        [System.Runtime.InteropServices.DllImport("User32.dll")]
        //        private extern bool GetLastInputInfo(ref LASTINPUTINFO Dummy);

        //        public uint GetIdleTime()
        //        {
        //            LASTINPUTINFO LastUserAction = new LASTINPUTINFO();
        //            LastUserAction.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(LastUserAction);
        //            GetLastInputInfo(ref LastUserAction);
        //            return ((uint)Environment.TickCount - LastUserAction.dwTime);
        //        }

        //        public bool HasBuyerPlanner(string pUserName)
        //        {
        //            bool isTrue = false;
        //            //DataTable dt = new DataTable();

        //            string sql = "SELECT userdef1 FROM [user] WHERE usr ='" + pUserName + "'";
        //            string buyer = string.Empty;
        //            buyer = Database.GetValue("NETCRM", sql, "userdef1");

        //            if (buyer.Trim() != string.Empty)
        //                isTrue = true;

        //            return isTrue;
        //        }

        //        public string GetBuyerPlanner(string pUserName)
        //        {
        //            DataTable dt = new DataTable();
        //            string mByrPlnr = string.Empty;
        //            string sql = "SELECT userdef1 FROM [user] WHERE usr = '" + pUserName + "'";
        //            dt = Database.GetDataTable("NETCRM", sql);

        //            if (dt.Rows.Count > 0)
        //                mByrPlnr = dt.Rows[0]["USERDEF1"].ToString().Trim();

        //            return mByrPlnr;
        //        }

        //        public bool BuyerHasItems(string pByrCode)
        //        {
        //            bool hasItems = false;
        //            DataTable dt = new DataTable();
        //            string sql = "EXEC sp_POFastMovingItems '" + pByrCode + "', 'ALL'";
        //            dt = Database.GetDataTable("NETCRM", sql);

        //            if (dt.Rows.Count > 0)
        //                hasItems = true;

        //            return hasItems;
        //        }

        //        /// <summary>
        //        /// Returns a list of the current logged-in user's sales rep numbers.
        //        /// </summary>
        //        /// <returns></returns>
        //        public List<string> GetRepNos()
        //        {
        //            return LoadRepData("repNo");
        //        }

        //        /// <summary>
        //        /// Returns a list of the current logged-in user's sales rep names.
        //        /// </summary>
        //        /// <returns></returns>
        //        public List<string> GetRepNames()
        //        {
        //            return LoadRepData("repName");
        //        }

        //        /// <summary>
        //        /// Returns a list of the current logged-in user's sales rep numbers and the rep numbers of the users that they manage.
        //        /// </summary>
        //        /// <returns></returns>
        //        public List<string> GetRepNosWithSubordinate()
        //        {
        //            return LoadRepDataWithSubordinate("repNo");
        //        }

        //        /// <summary>
        //        /// Returns a list of the current logged-in user's sales rep names and the rep names of the users that they manage.
        //        /// </summary>
        //        /// <returns></returns>
        //        public List<string> GetRepNamesWithSubordinate()
        //        {
        //            return LoadRepDataWithSubordinate("repName");
        //        }

        //        /// <summary>
        //        /// Loads the rep no's and names of the current logged in user.
        //        /// </summary>
        //        /// <param name="repType">Determine whether the output will be a repNo or a repName (repNo/repName).</param>
        //        /// <returns></returns>
        //        private List<string> LoadRepData(string repType)
        //        {
        //            if (repNo == null || repNames == null)
        //            {
        //                repNo = new List<string>();
        //                repNames = new List<string>();
        //                DataTable dt = new DataTable();
        //                string sql = "select UPPER(Slspsn_No) as Slspsn_No, Slspsn_Name from CusSlsPsn where usr ='" + mLoginName + "'";
        //                dt = Database.GetDataTable("NETCRM", sql);

        //                if (dt.Rows.Count > 0)
        //                {
        //                    foreach (DataRow r in dt.Rows)
        //                    {
        //                        repNo.Add(r["Slspsn_No"].ToString().Trim());
        //                        repNames.Add(r["Slspsn_Name"].ToString().Trim());
        //                    }

        //                }
        //            }

        //            if (repType.Equals("repNo"))
        //            {
        //                return repNo;
        //            }
        //            else if (repType.Equals("repName"))
        //            {
        //                return repNames;
        //            }
        //            else
        //                return null;
        //        }

        //        /// <summary>
        //        /// Loads the rep no's and names of the current logged in user and if the logged in user is a manager, including the rep nos/names of people that work under them.
        //        /// </summary>
        //        /// <param name="repType">Determine whether the output will be a repNo or a repName (repNo/repName).</param>
        //        private List<string> LoadRepDataWithSubordinate(string repType)
        //        {
        //            if (repNo == null || repNames == null)
        //            {
        //                repNo = new List<string>();
        //                repNames = new List<string>();
        //                DataTable dt = new DataTable();
        //                string sql = "select UPPER(Slspsn_No) as Slspsn_No, Slspsn_Name from CusSlsPsn where usr ='" + mLoginName + "' or manager = '" + mLoginName + "'";
        //                dt = Database.GetDataTable("NETCRM", sql);

        //                if (dt.Rows.Count > 0)
        //                {
        //                    foreach (DataRow r in dt.Rows)
        //                    {
        //                        repNo.Add(r["Slspsn_No"].ToString().Trim());
        //                        repNames.Add(r["Slspsn_Name"].ToString().Trim());
        //                    }

        //                }
        //            }

        //            if (repType.Equals("repNo"))
        //            {
        //                return repNo;
        //            }
        //            else if (repType.Equals("repName"))
        //            {
        //                return repNames;
        //            }
        //            else
        //                return null;
        //        }

        //        public string GetExactResIDFromSalesRep(string rep_no)
        //        {
        //            string SalesResID = string.Empty;
        //            string sql = "SELECT res_id FROM cusslspsn a INNER JOIN humres b ON a.slspsn_no = b.affix WHERE slspsn_no = left('" + rep_no + "', 3)";
        //            // SalesResID = Database.GetValue("NETCRM", sql, "res_id");
        //            SalesResID = Database.GetValue("NETCRM", sql, "res_id");
        //            return SalesResID;
        //        }

    }
}
