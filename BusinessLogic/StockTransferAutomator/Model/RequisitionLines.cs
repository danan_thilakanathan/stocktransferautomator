﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BusinessLogic.StockTransferAutomator.Model
{
    public class RequisitionLines : IDisposable
    {
        private DataTable dt = new DataTable("PO");

        public RequisitionLines()
        {
            MakePOTable();
        }
        public void setSortColumn(string columnName)
        {
            // Test Commit
            DataView dv = dt.DefaultView;
            dv.Sort = "artcode";
            dt = dv.ToTable();
            for (int index = 0; index < dt.Rows.Count; index++)
            {
                dt.Rows[index]["regel"] = index + 1;
            } //test
            // test branching strategy
        }
        public void Dispose()
        {
            dt.Dispose();
        }

        public DataTable DataTable
        {
            get
            {
                return dt;
            }
        }

        public int TotalRows
        {
            get { return dt.Rows.Count; }
        }

        public int TotalItems
        {
            get
            {
                int total = 0;

                foreach (DataRow dr in dt.Rows)
                {
                    total += Convert.ToInt32(dr["esr_aantal"]);
                }

                return total;
            }
        }

        public decimal TotalTax
        {
            get
            {
                decimal total = 0;

                foreach (DataRow dr in dt.Rows)
                {
                    total += Convert.ToDecimal(dr["esr_aantal"]) * Convert.ToDecimal(dr["prijs83"]) * (Convert.ToDecimal(dr["bdr_vat_val"]) / 100);
                }

                return total;
            }
        }

        /// <summary>
        /// Total Excluding Tax.
        /// </summary>
        public decimal TotalExcTax
        {
            get
            {
                decimal total = 0;

                foreach (DataRow dr in dt.Rows)
                {
                    total += Convert.ToDecimal(dr["esr_aantal"]) * Convert.ToDecimal(dr["prijs83"]);
                }

                return total;
            }
        }

        /// <summary>
        /// Total Including Tax.
        /// </summary>
        public decimal TotalIncTax
        {
            get
            {
                decimal total = 0;

                foreach (DataRow dr in dt.Rows)
                {
                    total += (Convert.ToDecimal(dr["esr_aantal"]) * Convert.ToDecimal(dr["prijs83"])) * (1 + (Convert.ToDecimal(dr["bdr_vat_val"])) / 100);
                }

                return total;
            }
        }

        /// <summary>
        /// Make datatable for storing quote line.
        /// </summary>
        private void MakePOTable()
        {
            DataColumn dc;

            dc = new DataColumn("regel");   //Line No
            dc.DataType = typeof(Int16);
            dt.Columns.Add(dc);

            dc = new DataColumn("magcode"); //Loc
            dc.DataType = typeof(String);
            dt.Columns.Add(dc);

            dc = new DataColumn("artcode"); //Item No
            dc.DataType = typeof(String);
            dt.Columns.Add(dc);

            dc = new DataColumn("oms45");   //Item Description
            dc.DataType = typeof(String);
            dt.Columns.Add(dc);

            dc = new DataColumn("esr_aantal"); //Qty
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("prijs83"); // Price Incl VAT in PH / Price Excl VAT in AU & HK
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("$Increase%");  //Unit price growth against the last order
            dc.DataType = typeof(String);
            dt.Columns.Add(dc);

            dc = new DataColumn("LowestProfitMargin");  //ProfitMargin_CusOrder
            dc.DataType = typeof(string);
            dc.DefaultValue = 0;
            dt.Columns.Add(dc);

            dc = new DataColumn("cus_no");  //cus_no
            dc.DataType = typeof(string);
            dc.DefaultValue = "";
            dt.Columns.Add(dc);

            //Qingyou Lu = 04/02/2011 -- Added Minimum Quantity, Maximum Quantity fields
            dc = new DataColumn("minqty");  //MinQty
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("maxqty");  //MaxQty
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            //Jiahao Xu-20120703--Add avg_cost and lowest profit margin
            dc = new DataColumn("avg_cost");    //avg_cost
            dc.DataType = typeof(double);
            dc.DefaultValue = 0;
            dt.Columns.Add(dc);

            dc = new DataColumn("unitcode");    //UOM
            dc.DataType = typeof(String);
            dt.Columns.Add(dc);

            dc = new DataColumn("Med"); //Med
            dc.DataType = typeof(String);
            dt.Columns.Add(dc);

            dc = new DataColumn("btw_code");    //Tax ID
            dc.DataType = typeof(String);
            dt.Columns.Add(dc);

            dc = new DataColumn("bdr_vat_val"); //bdr_vat_val
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("gst"); //GST
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            //if (Livingstone.Library.Database.DBaseUse == Livingstone.Library.Database.DBaseType.dbExactPH)
            //{
            //    dc = new DataColumn("PriceExclVAT"); // Price Excluding VAT in PH only
            //    dc.DataType = typeof(Double);
            //    dt.Columns.Add(dc);
            //}

            dc = new DataColumn("total");   //Total
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("prod_cat");    //Item Group
            dc.DataType = typeof(String);
            dt.Columns.Add(dc);

            dc = new DataColumn("specialfreeqty");  //Special Free Qty                                                                 
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("lastorderqty");    //LO Qty. The lastest order when you open this PO. not necessarily the order before this PO
            dc.DataType = typeof(Int32);
            dt.Columns.Add(dc);

            dc = new DataColumn("lastorderprice");  //LO Price. The lastest order when you open this PO. not necessarily the order before this PO
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("personauth");  //personauth
            dc.DataType = typeof(String);
            dt.Columns.Add(dc);

            dc = new DataColumn("authdate");    //authdate
            dc.DataType = typeof(String);
            dc.DefaultValue = "1900-01-01";
            dt.Columns.Add(dc);

            //Mark Rodel dela Rosa -- May 15, 2008 -- add additional columns into the data source
            dc = new DataColumn("landingcost"); //Landing Cost
            dc.DataType = typeof(Int16);
            dt.Columns.Add(dc);

            dc = new DataColumn("landingcost2");    //Landing Cost 2
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("cost");    //Cost
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("itemfreight"); //Item Freight
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("itemfumigation");  //Item Fumigation
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("itemhandling");    //Handling Charge
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("itemdiscount");    //Discount
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("on_cost_item");    //On-cost
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("on_cost_qty"); //On-cost for 1 Quantity
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("buy_qty"); //Buy Qty
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("sug_qty"); //Suggested Qty
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            //Mark Rodel dela Rosa - June 6, 2008 -- add the cubic field
            dc = new DataColumn("cubic");   //Cubic
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);
            //dc.ReadOnly = false;

            dc = new DataColumn("bo_qty");   //Cubic
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            dc = new DataColumn("prijs83_orig");    //OrigPrice
            dc.DataType = typeof(Double);
            dt.Columns.Add(dc);

            //Qingyou Lu = 04/02/2011 -- Added Minimum Quantity, Maximum Quantity fields (debug)
            //dc = new DataColumn("minqty");
            //dc.DataType = typeof(Double);
            //dt.Columns.Add(dc);

            //dc = new DataColumn("maxqty");
            //dc.DataType = typeof(Double);
            //dt.Columns.Add(dc);

            //dc = new DataColumn("item_class");
            //dc.DataType = typeof(String);
            //dt.Columns.Add(dc);

            //dc = new DataColumn("vendor_loc");
            //dc.DataType = typeof(String);
            //dt.Columns.Add(dc);

            dc = new DataColumn("Comments_Notes");  //Comments_Notes
            dc.DataType = typeof(Object);
            dt.Columns.Add(dc);

            dc = new DataColumn("ItemBuyerReply");  //ItemBuyerReply
            dc.DataType = typeof(String);
            dt.Columns.Add(dc);

            dc = new DataColumn("ProductComment");  //ProductComment
            dc.DataType = typeof(String);
            dt.Columns.Add(dc);

            dc = new DataColumn("ItemQtyReply");    //ItemQtyReply
            dc.DataType = typeof(String);
            dt.Columns.Add(dc);

            dc = new DataColumn("allocated");   //allocated
            dc.DataType = typeof(Double);
            dc.DefaultValue = 0;
            dt.Columns.Add(dc);

            dc = new DataColumn("month_usg");   //month_usg
            dc.DataType = typeof(Double);
            dc.DefaultValue = 0;
            dt.Columns.Add(dc);

            dc = new DataColumn("isQtyNeedAuth");   //isQtyNeedAuth
            dc.DataType = typeof(Int32);
            dc.DefaultValue = 0;
            dt.Columns.Add(dc);

            dc = new DataColumn("QtyAuthBy");   //QtyAuthBy
            dc.DataType = typeof(String);
            dc.DefaultValue = "";
            dt.Columns.Add(dc);


            dc = new DataColumn("QtyAuthDt");   //QtyAuthDt
            dc.DataType = typeof(String);
            dc.DefaultValue = "1900-01-01";
            dt.Columns.Add(dc);

            dc = new DataColumn("LOUnitPrice"); //LOUnitPrice. The order exactly before this order.
            dc.DataType = typeof(String);
            dc.DefaultValue = "";
            dt.Columns.Add(dc);

            dc = new DataColumn("auth_by");
            dc.DataType = typeof(String);
            dc.DefaultValue = "";
            dt.Columns.Add(dc);

            dc = new DataColumn("Lead Time Month");
            dc.DataType = typeof(Double);
            dc.DefaultValue = 0;
            dt.Columns.Add(dc);

            dc = new DataColumn("Stock Holding Month");
            dc.DataType = typeof(Double);
            dc.DefaultValue = 0;
            dt.Columns.Add(dc);


        }

        /// <summary>
        /// Check whether item is exists in the basket or not.
        /// </summary>
        /// <returns></returns>
        public bool IsInBasket(string itemNo)
        {
            bool b;

            DataView dv = new DataView(dt);
            dv.RowFilter = "artcode = '" + itemNo + "'";
            b = (dv.Count > 0);
            dv.Dispose();

            return b;
        }

        public bool DeleteLine(int line)
        {
            bool delete = false;

            if (dt.Rows.Count > 0)
            {
                    dt.Rows.RemoveAt(line);
                    CreateNewLineNo();
                    delete = true;
            }

            return delete;
        }

        /// <summary>
        /// Refresh quote line no.
        /// </summary>
        private void CreateNewLineNo()
        {
            int newLine = 1;
            
            foreach (DataRow dr in dt.Rows)
            {
                dr["regel"] = newLine;
                newLine++;
            }
        }

        public void CreateXML(ref XmlTextWriter tw, string Req_No, string PO_No)
        {
            foreach (DataRow dr in dt.Rows)
            {
                tw.WriteStartElement("reqlin");
                tw.WriteAttributeString("regel", dr["regel"].ToString());
                tw.WriteAttributeString("magcode", dr["magcode"].ToString().Trim());
                tw.WriteAttributeString("artcode", dr["artcode"].ToString().Trim());
                tw.WriteAttributeString("oms45", dr["oms45"].ToString().Trim());
                tw.WriteAttributeString("esr_aantal", dr["esr_aantal"].ToString());
                tw.WriteAttributeString("unitcode", dr["unitcode"].ToString());
                tw.WriteAttributeString("prijs83", dr["prijs83"].ToString().Trim());
                tw.WriteAttributeString("btw_code", dr["btw_code"].ToString());
                tw.WriteAttributeString("bdr_vat_val", dr["bdr_vat_val"].ToString().Trim());
                tw.WriteAttributeString("total", dr["total"].ToString().Trim());
                tw.WriteAttributeString("specialfreeqty", dr["specialfreeqty"].ToString().Trim());
                tw.WriteAttributeString("personauth", dr["personauth"].ToString());
                tw.WriteAttributeString("authdate", Convert.ToDateTime(dr["authdate"]).ToString("yyyy-MM-dd"));
                tw.WriteAttributeString("ordernr", PO_No);
                tw.WriteAttributeString("requisition", Req_No);
                tw.WriteAttributeString("landingcost", dr["landingcost"].ToString());
                tw.WriteAttributeString("landingcost2", dr["landingcost2"].ToString());
                tw.WriteAttributeString("cost", dr["cost"].ToString());
                tw.WriteAttributeString("itemfreight", dr["itemfreight"].ToString());
                tw.WriteAttributeString("itemfumigation", dr["itemfumigation"].ToString());
                tw.WriteAttributeString("itemhandling", dr["itemhandling"].ToString());
                tw.WriteAttributeString("itemdiscount", dr["itemdiscount"].ToString());
                tw.WriteAttributeString("on_cost_item", dr["on_cost_item"].ToString());
                tw.WriteAttributeString("on_cost_qty", dr["on_cost_qty"].ToString());
                tw.WriteAttributeString("buy_qty", dr["buy_qty"].ToString());
                tw.WriteAttributeString("sug_qty", dr["sug_qty"].ToString());
                tw.WriteAttributeString("cubic", Convert.ToDouble(dr["cubic"]).ToString("F7"));
                tw.WriteAttributeString("prod_cat", dr["prod_cat"].ToString().Trim());
                tw.WriteAttributeString("allocated", dr["allocated"].ToString());
                tw.WriteAttributeString("month_usg", dr["month_usg"].ToString().Trim());
                tw.WriteAttributeString("isQtyNeedAuth", dr["isQtyNeedAuth"].ToString().Trim());
                tw.WriteAttributeString("QtyAuthBy", dr["QtyAuthBy"].ToString().Trim());
                tw.WriteAttributeString("QtyAuthDt", dr["QtyAuthDt"].ToString().Trim());
                tw.WriteAttributeString("LOUnitPrice", dr["LOUnitPrice"].ToString().Trim());
                tw.WriteAttributeString("auth_by", dr["auth_by"].ToString().Trim());

                tw.WriteEndElement();
            }
        }
    }
}
