﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator.Model
{
    public enum Freight
    {
        FIS, FOB, ExWorks, CIF_Sea, CIF_LCL, CIF_Air, CF_FCL, CFR_FCL, CFR_Air, CFR_LCL, LocalCourier, VendorBillLocalCourier, CF_LCL, Others
    };

    public enum ContainerSize
    {
        Twenty, Forty, LCL, Local
    };

    public enum Payment
    {
        LetterOfCredit, PrePay, Collection, OnAccount, TTDeposit, TTPayment  
    };
    
    /// <summary>
    /// A dumb container for the Requisition data. Mainly used to pass around JSON data.
    /// </summary>
    public class Requisition
    {
        #region Private Variables

        #endregion


        #region Public Variables

        #region Requisition Common Fields (Top)

        public string SupplierNo { get; set; }
        public string BuyerPlanner { get; set; }
        public string PONumber { get; set; }
        public string RequisitionNumber { get; set; }
        public int ContactNumber { get; set; }

        #endregion

        #region  Requisition Common Fields (Bottom)

        public decimal OrderAmount { get; set; }
        public int Fumigation { get; set; }
        public decimal HandlingCharge { get; set; }
        public decimal TotalFreight { get; set; }
        public decimal TotalFreightOriginal { get; set; }
        public decimal TotalCubicMeters { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal Discount { get; set; }
        public decimal NetTotal { get; set; }
        public decimal TotalGST { get; set; }
        public decimal TotalIncludingGST { get; set; }
        public string CampaignCode { get; set; }
        public string ProcessTo { get; set; }
        public string ProcessStepNo { get; set; }
        public string StepDescription { get; set; }
        public bool AutoInternalTransfer { get; set; }

        #endregion

        #region Requisition Header

        public string SupplierName { get; set; }
        public string PODescription { get; set; }
        public string BatchNo { get; set; }
        public DateTime RequisitionDate { get; set; }
        public DateTime PromisedDate { get; set; }
        public DateTime ETADate { get; set; }
        public string CommentsToSupplier { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool RequisitionOnly { get; set; }
        public string Currency { get; set; }
        public string YourReference { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int NumberOfDays { get; set; }
        public int GoodsOnBoard { get; set; }
        public string POType { get; set; }
        public string InternalReferencePO { get; set; }
        public bool isWarehouseDelivery { get; set; }
        public bool isStandingRequisition { get; set; }
        public string Location { get; set; }
        public string Warehouse { get; set; }
        public string WarehouseNotes { get; set; }
        public string InternalComments { get; set; }
        public string OrderNo { get; set; }
        public string CustomerNumber { get; set; }
        public string Name { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string Suburb { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string PhoneNumber { get; set; }

        #endregion

        #region RequisitionLines
        public string ItemNo { get; set; }
        public string UnitCode { get; set; }
        public int QtyOnHand { get; set; }
        public int QtyOnHandAll { get; set; }
        public string POMultiple { get; set; }
        public int MinimumQty { get; set; }
        public int ReorderBuyQty { get; set; }
        public int BuyQty { get; set; }
        public int CustomerAllocated { get; set; }
        public decimal Cubic { get; set; }
        public decimal TMUY { get; set; }
        public decimal TMUYAll { get; set; }
        public string Priority { get; set; }
        public string Class { get; set; }
        public string ItemDescription { get; set; }
        public decimal ItemVendorPrice { get; set; }
        public string LastPONumber { get; set; }
        public DateTime LastPODate { get; set; }
        public decimal LastOrderUnitPrice { get; set; }
        public decimal NegotiatedPrice { get; set; }
        public int LastOrderQty { get; set; }
        public decimal TotalPrice { get; set; }
        public string AuthorisedBy { get; set; }
        public string LT { get; set; }
        public string SH { get; set; }
        public string CommentsNotes { get; set; }
        public string VendorItemNo { get; set; }
        public string ProductCategory { get; set; }
        public bool isMedicalProduct { get; set; }
        public int RRPQty1 { get; set; }
        public int RRPQty2 { get; set; }
        public int RRPQty3 { get; set; }
        public decimal RRPPrice1 { get; set; }
        public decimal RRPPrice2 { get; set; }
        public decimal RRPPrice3 { get; set; }
        public DataTable ItemLines { get; set; }
        public RequisitionLines POBasket { get; set; }

        #endregion

        #region Freight

        public DateTime ArrivalDate { get; set; }
        public DateTime FreightDeliveryDate { get; set; }
        public Freight FreightType  { get; set; }
        public string Basis { get; set; }
        public string FreightCharge { get; set; }
        public string FreightChargeOriginal { get; set; }
        public string FreightExpiry { get; set; }
        public string PaymentTerm { get; set; }
        public ContainerSize ContainerSize { get; set; }
        public string OtherDigit { get; set; }
        public int NoOfContainers { get; set; }
        public int NoOfCartons { get; set; }
        public string FreightForwarderBillNo { get; set; }
        public string TelephoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string VesselNumber { get; set; }
        public string CourierCompanyName { get; set; }
        public string ContainerNumber { get; set; }
        public string Email { get; set; }

        #endregion

        #region Payments

        public Payment PaymentType { get; set; }
        //public DataTable PaymentData { get; set; }
        public string ProformaInvoice { get; set; }
        public string PaymentCompleted { get; set; }
        public string BankConfirmedLetterOfCredit { get; set; }
        public DateTime PaymentArrivalDate { get; set; }
        public DateTime PaymentDeliveryDate { get; set; }

        #endregion


        #region Extras

        public bool IsNewRequisition { get; set; }

        #endregion

        #endregion
    }
}
