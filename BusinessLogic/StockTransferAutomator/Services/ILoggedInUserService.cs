﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator.Services
{
    public interface ILoggedInUserService
    {
         string Screen { get; set; }

        /// <summary>
        /// Get or set Use Master status.
        /// </summary>
        bool IsUseMaster { get; set; }

        /// <summary>
        /// Get or set LoginName
        /// </summary>
        string LoginName { get; set; }

        /// <summary>
        /// Get or set Name
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Get or set Office
        /// </summary>
        string Office { get; set; }

        /// <summary>
        /// Get or set LoginCompany
        /// </summary>
         string LoginCompany { get; set; }

        /// <summary>
        /// Get or set LoginServer
        /// </summary>
        string LoginServer { get; set; }

        /// <summary>
        /// Get or set LoginPassword
        /// </summary>
        string LoginPassword { get; set; }

        /// <summary>
        /// Get or set Email
        /// </summary>
        string Email { get; set; }

        /// <summary>
        /// Get or set Active Directory Username
        /// </summary>
        string ADUser { get; set; }

        // global session ID for user till they logoff - added by Malin

        string gblLSessionID { get; set; }

        /// <summary>
        /// Get or set Description
        /// </summary>
        string Description { get; set; }

        string DummySetting { set; }


        bool HasPermission(string permissionName);
        bool HasRole(string roleName);

        void RetrievePermission(string username);
        void RetrieveRole(string username);

    }
}
