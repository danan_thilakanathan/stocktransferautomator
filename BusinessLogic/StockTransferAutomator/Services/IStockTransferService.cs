﻿using Database.DatabaseRepositories.Model;
using Database.DataRepositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator.Services
{
    public interface IStockTransferService
    {

        string GenerateStockTransferID(string user);
        string GetInternalCustomer();
        bool UpdateCustomer(string customer);

        /// <summary>
        /// Carries out the task of transferring stock from one location to another.
        /// </summary>
        /// <param name="stockTransferId">StockTransfer ID to transfer items</param>
        /// <param name="from">Location to transfer from</param>
        /// <param name="to">Location to transfer to</param>
        /// <param name="priority">The priority of items to retrieve. Leaving it blank will include all priority items.</param>
        /// <param name="stockTransferRepository">The repository for Stock Transfers</param>
        void RunStockTransfer(string stockTransferId, string from, string to, string priority, string comments, string user);

        List<string> RetrieveAllWarehouses();
        DataTable GetItemsToTransfer(string fromWarehouse, string toWarehouse, string priority);
        string GenerateQuote(string stockTransferId, string comments, DataTable items);
        StockTransferEntry GetEntryHeader(string stockTransferId);
        DataTable GetEntryLines(string stockTransferId);
        string SaveEntry(string stockTransferId, string fromLocation, string toLocation, string priority, string user,
            string quoteNo, string nOrderNo, string eOrderNo,
            string requisition, string poNumber,
            DataTable items);

    }
}
