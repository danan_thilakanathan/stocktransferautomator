﻿using Database.DatabaseRepositories.Model;
using Database.DataRepositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator.Services
{
    public class StockTransferService : IStockTransferService
    {
        private IStockTransferRepository _stockTransferRepository;
        private IGetItemsToTransferService _getItemsService;
        private IGenerateQuoteService _generateQuoteService;

        public StockTransferService()
        {
            //_stockTransferRepository = new StockTransferRepository();
            //_getItemsService = new GetItemsToTransferService(_stockTransferRepository);
            //_generateQuoteService = new GenerateQuoteService(_stockTransferRepository);

        }

        public StockTransferService(IStockTransferRepository stockTransferRepository, IGetItemsToTransferService getItemsService, IGenerateQuoteService generateQuoteService)
        {

            if (stockTransferRepository != null)
                _stockTransferRepository = stockTransferRepository;
            else
                _stockTransferRepository = new StockTransferRepository();

            if (getItemsService != null)
                _getItemsService = getItemsService;
            else
                _getItemsService = new GetItemsToTransferService(_stockTransferRepository);

            if (generateQuoteService != null)
                _generateQuoteService = generateQuoteService;
            else
                _generateQuoteService = new GenerateQuoteService(_stockTransferRepository);
        }

        public void RunStockTransfer(string stockTransferId, string from, string to, string priority, string comments, string user)
        {
            DataTable items = GetItemsToTransfer(from, to, priority);  //later switch from DataTable to a custom Item class

            string entryId = AddStockTransferEntry(stockTransferId, user);

            string quote = GenerateQuote(entryId, comments, items);

            string requisition = GenerateRequisition(entryId, quote);

            if (ApproveQty(requisition))
            {
                if (ApprovePrice(entryId, requisition))
                {
                    UpdateQuote(quote);
                    ConvertQuoteToOrder(entryId, quote);
                }
            }
                     
        }

        public List<string> RetrieveAllWarehouses()
        {
            return _stockTransferRepository.RetrieveAllWarehouses();
        }

        public string AddStockTransferEntry(string stockTransferId, string user)
        {
            return _stockTransferRepository.AddStockTransferEntry(stockTransferId, user);
        }

        public DataTable GetItemsToTransfer(string from, string to, string priority)
        {
            return _getItemsService.GetItemsToTransfer(from, to, priority);
        }

        public string GenerateQuote(string stockTransferId, string comments, DataTable items)
        {
            return _generateQuoteService.GenerateQuote(stockTransferId, comments, items);
        }

        public string SaveEntry(string stockTransferId, string fromLocation, string toLocation, string priority, string user,
            string quoteNo, string nOrderNo, string eOrderNo,
            string requisition, string poNumber,
            DataTable items)
        {
            StockTransferEntry entry = new StockTransferEntry();
            entry.StockTransferID = stockTransferId;
            entry.FromWarehouse = fromLocation;
            entry.ToWarehouse = toLocation;
            entry.Priority = priority;
            entry.User = user;
            entry.QuoteNo = quoteNo;
            entry.NetcrmOrderNo = nOrderNo;
            entry.ExactOrderNo = eOrderNo;
            entry.Requisition = requisition;
            entry.PONumber = poNumber;
            entry.Items = items;

            return _stockTransferRepository.SaveEntry(entry);
        }

        private string GenerateRequisition(string a, string b)
        {
            throw new NotImplementedException();
        }

        private bool ApproveQty(string b)
        {
            throw new NotImplementedException();
        }

        private bool ApprovePrice(string a, string b)
        {
            throw new NotImplementedException();
        }

        private void UpdateQuote(string a)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Convert the generated Quote to an Order.
        /// </summary>
        /// <returns>Details of the converted order</returns>
        public DataTable ConvertQuoteToOrder(string entryId, string quote)
        {
            DataTable orders;
            if (!string.IsNullOrEmpty(quote))
                orders = _stockTransferRepository.ConvertQuoteToOrder(quote);
            else
                throw new Exception("Order Conversion Failed. No quote number generated.");

            return orders;
        }

        public string GenerateStockTransferID(string user)
        {
            return _stockTransferRepository.GenerateStockTransferID(user);
        }

        public string GetInternalCustomer()
        {
            return _stockTransferRepository.GetInternalCustomer();
        }

        public bool UpdateCustomer(string customer)
        {
            return _stockTransferRepository.UpdateInternalCustomer(customer);
        }

        public StockTransferEntry GetEntryHeader(string stockTransferId)
        {
            if (string.IsNullOrEmpty(stockTransferId)) throw new Exception("Stock Transfer ID not entered"); 
            return _stockTransferRepository.GetEntryHeader(stockTransferId);
        }

        public DataTable GetEntryLines(string stockTransferId)
        {
            if (string.IsNullOrEmpty(stockTransferId)) throw new Exception("Stock Transfer ID not entered");

            return _stockTransferRepository.GetEntryLines(stockTransferId);
        }
    }
}
