﻿using BusinessLogic.StockTransferAutomator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator.Services
{
    public interface ILoadRequisitionFromQuoteService
    {
        Requisition LoadFromQuote(string quote, string supplierNo);
    }
}
