﻿using Database.DataRepositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator.Services
{
    public class GetItemsToTransferService : IGetItemsToTransferService
    {
        private IStockTransferRepository _stockTransferRepository;

        public GetItemsToTransferService(IStockTransferRepository stockTransferRepository)
        {
            if (stockTransferRepository != null)
                _stockTransferRepository = stockTransferRepository;
            else
                _stockTransferRepository = new StockTransferRepository();
        }

        DataTable IGetItemsToTransferService.GetItemsToTransfer(string fromWarehouse, string toWarehouse, string priority)
        {
            return _stockTransferRepository.GetItemsToTransfer(fromWarehouse, toWarehouse, priority);
        }
    }
}
