﻿using BusinessLogic.StockTransferAutomator.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator.Services
{
    public class LoggedInUserService : ILoggedInUserService
    {
        private string mLoginName;
        private string mLoginPassword;
        private string mLoginCompany;
        private string mLoginServer;
        private string mgblLSessionID;
        private string mName;
        private string mOffice;
        private string mScreen;
        private string mDescription;
        public int UpdatePrice;
        private string mDummySetting = string.Empty;
        private DataTable mDataTable = new DataTable();
        private DataTable rDataTable = new DataTable();
        private List<string> mLabFeatures;
        private string email;
        private string mADUsr;
        private List<string> repNo;
        private List<string> repNames;


        //Public Events
        public delegate void LabsChangedHandler(string labsFeature, bool enabled);
        public event LabsChangedHandler LabsChanged;

        public bool isKBinit = false;
        public bool isTesting = false;
        public bool isITuser = false;
        public string nusr = "";
        public string emailaddr = "";
        public string displayname = "";

        /// <summary>
        /// Get/set Screen Menu.
        /// </summary>
        public string Screen
        {
            get { return mScreen; }
            set { mScreen = value; }
        }

        /// <summary>
        /// Get or set Use Master status.
        /// </summary>
        public bool IsUseMaster { get; set; }

        /// <summary>
        /// Get or set LoginName
        /// </summary>
        public string LoginName
        {
            get { return mLoginName; }
            set { mLoginName = value; }
        }
        /// <summary>
        /// Get or set Name
        /// </summary>
        public string Name
        {
            get { return mName; }
            set { mName = value; }
        }

        /// <summary>
        /// Get or set Office
        /// </summary>
        public string Office
        {
            get { return mOffice; }
            set { mOffice = value; }
        }
        /// <summary>
        /// Get or set LoginCompany
        /// </summary>
        public string LoginCompany
        {
            get { return mLoginCompany; }
            set { mLoginCompany = value; }
        }

        /// <summary>
        /// Get or set LoginServer
        /// </summary>
        public string LoginServer
        {
            get { return mLoginServer; }
            set { mLoginServer = value; }
        }

        /// <summary>
        /// Get or set LoginPassword
        /// </summary>
        public string LoginPassword
        {
            get { return mLoginPassword; }
            set { mLoginPassword = value; }
        }

        /// <summary>
        /// Get or set Active Directory Username
        /// </summary>
        public string ADUser
        {
            get { return mADUsr; }
            set { mADUsr = value; }
        }

        // global session ID for user till they logoff - added by Malin

        public string gblLSessionID
        {
            get { return mgblLSessionID; }
            set { mgblLSessionID = value; }
        }

        /// <summary>
        /// Get or set Description
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set { mDescription = value; }
        }

        public string DummySetting
        {
            set { mDummySetting = value; }
        }

        public string Email { get => email; set => email = value; }

        public bool HasPermission(string permissionName)
        {
            bool b = false;

            if (mDataTable.Columns["permission"] != null)
            {
                mDataTable.TableName = "permission";
                DataView dv = new DataView();
                dv.Table = mDataTable;
                dv.RowFilter = "Permission = '" + permissionName + "'";
                if (dv.Count == 1) b = true;
                dv.Dispose();

                //string YesOrNo;
                //if (b) YesOrNo = "Y";
                //else YesOrNo = "N";
                //NETCRM ln = new NETCRM();
                //ln.Execute("insert into AppLogs(usr, permission, [time],valid) values('"+mLoginName+"','"+PermissionName+"',getdate(),'"+YesOrNo+"')");
                //ln.Dispose();
            }

            return b;
        }

        public bool HasRole(string roleName)
        {
            bool b = false;

            if (rDataTable.Columns["role"] != null)
            {
                rDataTable.TableName = "role";
                DataView dv = new DataView();
                dv.Table = rDataTable;
                dv.RowFilter = "role = '" + roleName + "'";
                if (dv.Count == 1) b = true;
                dv.Dispose();
            }
            return b;
        }

        /// <summary>
        /// Read Permission from database for a particular user name
        /// when user login and keep this in a data table.
        /// </summary>
        /// <param name="userName"></param>		
        public void RetrievePermission(string UserName)
        {
            //Clear any old entries in the data table (mDataTable)
            mDataTable.Clear();

            //Retrieve the new values from the database and store them into the data table (mDataTable)
            NETCRM ln = new NETCRM();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter adap = new SqlDataAdapter();
            string sql;

            sql = @"
SELECT a.permission, b.screen, b.name, b.office
FROM UserPermission a
	LEFT OUTER JOIN [user] b ON a.usr = b.usr 
WHERE a.usr = @username AND a.PermissionValue = 1 ";

            com.Connection = ln.GetSqlConnection();
            com.CommandType = CommandType.Text;
            com.CommandText = sql;
            com.Parameters.AddWithValue("@username", UserName);

            adap.SelectCommand = com;
            adap.Fill(mDataTable);

            adap.Dispose();
            com.Dispose();
            ln.Dispose();
            mLoginName = UserName;

            if (mDataTable.Rows.Count > 0)
            {
                mScreen = mDataTable.Rows[0]["screen"].ToString().Trim();
                mName = mDataTable.Rows[0]["name"].ToString().Trim();
                mOffice = mDataTable.Rows[0]["office"].ToString().Trim();
            }
            else
                mScreen = string.Empty;
            adap.Dispose();
            com.Dispose();
            ln.Dispose();
            if (mScreen.Trim() == string.Empty)
            {
                sql = ("SELECT DISTINCT * from [user] " +
                    "WHERE usr = '" + UserName + "'");
                mScreen = Database.GetValue("NETCRM", sql.Trim(), "screen").Trim();
            }
        }

        /// <summary>
        /// Read Role from database for a particular user name
        /// when user login and keep this in a data table.
        /// </summary>
        /// <param name="userName"></param>	
        public void RetrieveRole(string UserName)
        {
            rDataTable.Clear();
            NETCRM ln = new NETCRM();
            SqlCommand com = new SqlCommand();

            SqlDataAdapter adap = new SqlDataAdapter();
            string sql = string.Empty;
            try
            {

                sql = @"
SELECT role 
FROM userrole a
	LEFT OUTER JOIN [user] b ON a.usr = b.usr
WHERE a.usr = @username";


                com.Connection = ln.GetSqlConnection();
                com.CommandType = CommandType.Text;
                com.CommandText = sql;
                com.Parameters.AddWithValue("@username", UserName);

                adap.SelectCommand = com;
                adap.Fill(rDataTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot retrieve User Role (RetrieveRole(string UserName)) : " + ex.Message);
            }
            finally
            {
                adap.Dispose();
                com.Dispose();
                ln.Dispose();
            }
        }


    }
}
