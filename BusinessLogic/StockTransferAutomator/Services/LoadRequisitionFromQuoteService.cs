﻿using BusinessLogic.StockTransferAutomator.Model;
using Database.DataRepositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator.Services
{

    public class LoadRequisitionFromQuoteService : ILoadRequisitionFromQuoteService
    {
        private IRequisitionRepository _requisitionRepository;

        public Requisition requisition { get; set; }

        public LoadRequisitionFromQuoteService(IRequisitionRepository requisitionRepository)
        {
            _requisitionRepository = requisitionRepository;
        }

        public Requisition LoadFromQuote(string quote, string supplierNo)
        {
            requisition = InitialiseRequisition();

            DataTable quoteLines = _requisitionRepository.GetQuoteLinesFromQuote(quote);

            requisition.SupplierNo = supplierNo;

            CheckMissingItems(quoteLines, supplierNo);

            //requisition.ItemLines = SetDataGridPO(requisition.ItemLines);
            //import PO lines by simulating user operations
            foreach (DataRow dr in quoteLines.Rows)
            {
                AddItem(); //click Add button
                requisition.ItemNo = dr["ItemNo"].ToString();  //type in the item no
                ItemNonFocused(); //hit Tab key
                //lookItem_Leave(lookItem, null); //hit Tab key
                ItemRefresh();
                //lookItem.RefreshLookup();
                requisition.CommentsNotes = "Internal Order."; //type in notes
                requisition.BuyQty = Int32.Parse(dr["Qty"].ToString());  //type in Buy Qty
                SaveRequisition(); //Click Save Button
                //btnSave_Click(btnSave, null);   //click Save button

            } 
            requisition.CommentsToSupplier = quote;
            requisition.HandlingCharge = _requisitionRepository.GetHandlingCharge(supplierNo); //Formerly SetHandlingCharge()


            return requisition;
        }


        public Requisition InitialiseRequisition()
        {
            requisition = new Requisition();

            requisition.IsNewRequisition = true;
            SetBuyerDefault();
            CheckFilesAccess();
            SetPayments();
            //SetBudget();
            //SetPOFiles();
            //RemoveTabs();
            //validateTab = true;
            //lookItem.IsFormPO = true;
            //GetControlProperties();
            //SetControlsBasedOnPermission();
            //dteExpiryDate.Value = dtpOrder.Value.AddMonths(3);
            //if (priceScaleTextBoxes == null)
            //    priceScaleTextBoxes = new List<TextBox[]>()
            //    {
            //        new TextBox[]{txtRRPQty1, txtRRPPrice1},
            //        new TextBox[]{txtRRPQty2, txtRRPPrice2},
            //        new TextBox[]{txtRRPQty3, txtRRPPrice3}
            //    };
            //setExpCert();

            return requisition;
        }

        private void SetPayments()
        {
            throw new NotImplementedException();
        }

        private void CheckFilesAccess()
        {
            throw new NotImplementedException();
        }

        private void SetBuyerDefault()
        {
            //requisition.BuyerPlanner = _requisitionRepository.GetDefaultBuyerPlanner(string user)
        }

        private void CheckMissingItems(DataTable quoteLines, string supplierNo)
        {
            List<string> missingItems = new List<string>();
            foreach (DataRow dr in quoteLines.Rows)
            {
                string itemNo = dr["ItemNo"].ToString();
                requisition.ItemNo = itemNo;
                DataTable dt = _requisitionRepository.GetItemDataFromVendor(itemNo, supplierNo);
                if (dt.Rows.Count == 0)
                    missingItems.Add(itemNo);
            }

            if (missingItems.Count > 0)
            {
                string strItems = string.Join(", \n    ", missingItems.ToArray());
                string msg =
@"Operation failed because the following items are not related to the Supplier. 
Please create the relationship in Item Vendor Master and try again.
Item No:
    "
                    + strItems;
                throw new Exception("Failed - Supplier No:" + supplierNo + ": " + msg);
            }
        }

        //Grid Columnsd
        //private DataTable SetDataGridPO(DataTable requisitionLines)
        //{
        //    requisitionLines.Clear();
        //    bool hasPermission = !PermissionReader.HasPermission("TestRequisitionPermission");//Used to hide columns to users who has permission
        //    requisitionLines.Columns.Add("Line");
        //    requisitionLines.Columns.Add("Loc");
        //    requisitionLines.Columns.Add("Item No");
        //    requisitionLines.Columns.Add("Item Description");
        //    requisitionLines.Columns.Add("Qty");

        //    if (Livingstone.Library.Database.DBaseUse != Livingstone.Library.Database.DBaseType.dbExactPH)
        //        requisitionLines.Columns.Add("Price (Excl VAT)"); // Without GST
        //    else
        //        requisitionLines.Columns.Add("Price (Incl VAT)", (hasPermission ? 100 : 0), true, HorizontalAlignment.Right, "F4"));

        //    requisitionLines.Columns.Add("$Increase%");

        //    requisitionLines.Columns.Add("Profit%");
        //    requisitionLines.Columns.Add("cus_no");

        //    #region Qingyou Lu -- 04/02/2011 -- Added into data grid for min qty, max qty
        //    requisitionLines.Columns.Add("MinQty");
        //    requisitionLines.Columns.Add("MaxQty");
        //    #endregion

        //    #region Jiahao Xu -- 20120703 -- Added into data grid for avg_cost,cheapest price in current order
        //    requisitionLines.Columns.Add("avg_cost");
        //    //requisitionLines.Columns.Add("ProfitMargin_CusOrder", 130, true, HorizontalAlignment.Left));
        //    //requisitionLines.Columns.Add("cus_no", 65, true, HorizontalAlignment.Left));            
        //    #endregion

        //    requisitionLines.Columns.Add("UOM");

        //    requisitionLines.Columns.Add("Med");

        //    //requisitionLines.Columns.Add("Price", (hasPermission ? 70 : 0), true, HorizontalAlignment.Right, "F4"));
        //    //requisitionLines.Columns.Add("Tax ID", (hasPermission ? 50 : 0), true, HorizontalAlignment.Right));
        //    //requisitionLines.Columns.Add("bdr_vat_val", 0, true));
        //    if (Livingstone.Library.Database.DBaseUse != Livingstone.Library.Database.DBaseType.dbExactPH)
        //    {
        //        //requisitionLines.Columns.Add("Price", (hasPermission ? 70 : 0), true, HorizontalAlignment.Right, "F4")); // Without GST
        //        requisitionLines.Columns.Add("Tax ID", (hasPermission ? 50 : 0), true, HorizontalAlignment.Right));
        //        requisitionLines.Columns.Add("bdr_vat_val", 0, true));
        //        requisitionLines.Columns.Add("GST", (hasPermission ? 60 : 0), true, HorizontalAlignment.Right, "F4"));
        //        requisitionLines.Columns.Add("Total", (hasPermission ? 70 : 0), true, HorizontalAlignment.Right, "F4"));
        //    }
        //    else
        //    {
        //        #region Old Code - May 31, 2011
        //        /*if (txtCurrency.Text.Trim().ToUpper() == "PHP")
        //        {
        //            requisitionLines.Columns.Add("Tax ID", (hasPermission ? 50 : 0), true, HorizontalAlignment.Right));
        //            requisitionLines.Columns.Add("bdr_vat_val", 0, true));
        //            requisitionLines.Columns.Add("GST", "Ex VAT price", (hasPermission ? 80 : 0), true, HorizontalAlignment.Right, "F4"));
        //            requisitionLines.Columns.Add("Total", "Incl VAT price", (hasPermission ? 90 : 0), true, HorizontalAlignment.Right, "F4"));
        //        }
        //        else
        //        {
        //            requisitionLines.Columns.Add("Tax ID", (hasPermission ? 0 : 0), true, HorizontalAlignment.Right));
        //            requisitionLines.Columns.Add("bdr_vat_val", 0, true));
        //            requisitionLines.Columns.Add("GST", 0, true, HorizontalAlignment.Right, "F4"));
        //            requisitionLines.Columns.Add("Total", "Incl VAT price", (hasPermission ? 90 : 0), true, HorizontalAlignment.Right, "F4"));
        //        }*/
        //        #endregion

        //        #region Modified Code - June 3, 2011
        //        //requisitionLines.Columns.Add("Price", "Price (Incl VAT)", (hasPermission ? 100 : 0), true, HorizontalAlignment.Right, "F4"));
        //        requisitionLines.Columns.Add("Tax ID", ((hasPermission && txtCurrency.Text.Trim().ToUpper() == "PHP") ? 50 : 0), true, HorizontalAlignment.Right));
        //        requisitionLines.Columns.Add("bdr_vat_val", 0, true));
        //        requisitionLines.Columns.Add("GST", "VAT", (hasPermission ? 80 : 0), true, HorizontalAlignment.Right, "F4"));
        //        requisitionLines.Columns.Add("PriceExclVAT", "Price (Excl VAT)", (hasPermission ? 100 : 0), true, HorizontalAlignment.Right, "F4"));
        //        requisitionLines.Columns.Add("Total", (hasPermission ? 90 : 0), true, HorizontalAlignment.Right, "F4"));
        //        #endregion
        //    }
        //    requisitionLines.Columns.Add("Item Group", 90, true, HorizontalAlignment.Right, "F4"));
        //    requisitionLines.Columns.Add("Special Free Qty", 50, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("LO Qty", 50, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("LO Price", (hasPermission ? 70 : 0), true, HorizontalAlignment.Right, "F4"));
        //    requisitionLines.Columns.Add("personauth", 0, true));
        //    requisitionLines.Columns.Add("authdate", 0, true));
        //    //Mark Rodel dela Rosa -- May 14, 2008 -- add columns into the grid for the computation of costs
        //    requisitionLines.Columns.Add("Landing Cost (%)", (hasPermission ? 100 : 0), false, HorizontalAlignment.Right, "F0"));//15
        //    requisitionLines.Columns.Add("Landing Cost 2", 0, true));
        //    requisitionLines.Columns.Add("Cost", 0, true));
        //    requisitionLines.Columns.Add("Item Freight", 0, true));
        //    requisitionLines.Columns.Add("Item Fumigation", 0, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("Handling Charge", 0, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("Discount", 0, true));
        //    requisitionLines.Columns.Add("On-cost", (hasPermission ? 80 : 0), true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("On-cost for 1 Quantity", (hasPermission ? 120 : 0), true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("Buy Qty", 0, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("Suggested Qty", 0, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("Cubic", 0, false, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("BO Qty", 50, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("OrigPrice", 0, true));
        //    requisitionLines.Columns.Add("Comments_Notes", 0, true));
        //    requisitionLines.Columns.Add("ItemBuyerReply", 0, true));
        //    requisitionLines.Columns.Add("ProductComment", 0, true));
        //    requisitionLines.Columns.Add("ItemQtyReply", 0, true));

        //    requisitionLines.Columns.Add("allocated", 60, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("month_usg", 60, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("isQtyNeedAuth", 0, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("QtyAuthBy", 0, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("QtyAuthDt", 0, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("LOUnitPrice", 0, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("auth_by", 0, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("Lead Time Month", 50, true, HorizontalAlignment.Right));
        //    requisitionLines.Columns.Add("Stock Holding Month", 50, true, HorizontalAlignment.Right));

        //    requisitionLines.DataSource = basket.DataTable;

        //    return requisitionLines;
        //}

        private void AddItem() { }

        private void ItemNonFocused() { }

        private void ItemRefresh() { }

        private void SetHandlingCharge()
        {
            throw new NotImplementedException();
        }

        private void SaveRequisition()
        {
            throw new NotImplementedException();
        }

    }
}
