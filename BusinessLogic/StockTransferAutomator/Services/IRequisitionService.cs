﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator.Services
{
    public interface IRequisitionService
    {
        string GenerateRequisition(string quoteNo);
        bool ApproveQty(string requisition);
        string ApprovePrice(string requisition);
    }
}
