﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator.Services
{
    public class RequisitionService : IRequisitionService
    {
        /// <summary>
        /// Generate a requisition based on the given quote.
        /// </summary>
        /// <param name="quoteNo">Quote to generate requisition from</param>
        /// <returns>Requisition number</returns>
        public string ApprovePrice(string requisition)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Approve the Step 2 requisition
        /// </summary>
        /// <param name="requisition">Requisition number to approve qty</param>
        /// <returns>True if approved</returns>
        public bool ApproveQty(string requisition)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Approve the Step 3 requisition
        /// </summary>
        /// <param name="requisition">Requisition number to approve price</param>
        /// <returns>PO Number</returns>
        public string GenerateRequisition(string quoteNo)
        {
            throw new NotImplementedException();
        }
    }
}
