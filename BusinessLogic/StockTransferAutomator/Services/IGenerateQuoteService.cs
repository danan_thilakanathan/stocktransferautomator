﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator.Services
{
    public interface IGenerateQuoteService
    {
        string GenerateQuote(string stockTransferId, string comments, DataTable items);
    }
}
