﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DataRepositories;

namespace BusinessLogic.StockTransferAutomator.Services
{
    public class GenerateQuoteService : IGenerateQuoteService
    {
        private IStockTransferRepository _stockTransferRepository;

        public GenerateQuoteService()
        {
            _stockTransferRepository = new StockTransferRepository();
        }

        public GenerateQuoteService(IStockTransferRepository stockTransferRepository)
        {
            _stockTransferRepository = stockTransferRepository;
        }

        public string GenerateQuote(string stockTransferId, string comments, DataTable items)
        {
                return _stockTransferRepository.GenerateQuote(stockTransferId, comments, items);

        }
    }
}
