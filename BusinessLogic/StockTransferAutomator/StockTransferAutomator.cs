﻿using Database.DataRepositories;
using Livingstone.OrderLibrary;
using BusinessLogic.StockTransferAutomator.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator
{

    public class StockTransferAutomator
    {
        #region Fields
        private IRequisitionRepository _requisitionRepository;
        private IStockTransferRepository _stockTransferRepository;
        private IStockTransferService _stockTransferService;
        private string stockTransferID;
        private DataTable itemsToTransfer;
        private string quote;
        private string netCRMOrderNo;
        private string exactOrderNo;
        private string requisition;
        private string poNumber;

        private string customerNo;
        private List<string> warehouseLocations;

        private string entryID;
        private string fromWarehouse;
        private string toWarehouse;
        private string priority;
        #endregion

        #region Properties
        public string StockTransferId { get => stockTransferID; set => stockTransferID = value; }
        public string EntryID { get => entryID; set => entryID = value; }
        public DataTable ItemsToTransfer { get => itemsToTransfer; set => itemsToTransfer = value; }
        public string Quote { get => quote; set => quote = value; }
        public string NetCRMOrderNo { get => netCRMOrderNo; set => netCRMOrderNo = value; }
        public string ExactOrderNo { get => exactOrderNo; set => exactOrderNo = value; }
        public string Requisition { get => requisition; set => requisition = value; }
        public string PoNumber { get => poNumber; set => poNumber = value; }
        public string CustomerNo { get => customerNo; set => customerNo = value; }
        public List<string> WarehouseLocations { get => warehouseLocations; set => warehouseLocations = value; }
        public string FromWarehouse { get => fromWarehouse; set => fromWarehouse = value; }
        public string ToWarehouse { get => toWarehouse; set => toWarehouse = value; }
        public string Priority { get => priority; set => priority = value; }

        #endregion


        #region Constructor
        public StockTransferAutomator(IRequisitionRepository requisitionRepository, IStockTransferRepository stockTransferRepository, IStockTransferService runStockTransferService)
        {
            if (requisitionRepository != null)
                _requisitionRepository = requisitionRepository;
            else
                _requisitionRepository = new RequisitionRepository();

            if (stockTransferRepository != null)
                _stockTransferRepository = stockTransferRepository;
            else
                _stockTransferRepository = new StockTransferRepository();

            if (runStockTransferService != null)
                _stockTransferService = runStockTransferService;
            else
                _stockTransferService = new StockTransferService(_stockTransferRepository, null, null);
        }


        #endregion

        #region Public Functions

        /// <summary>
        /// Run Stock Transfer
        /// </summary>
        /// <returns>True if Stock Transfer is successful</returns>
        public bool RunStockTransfer(string priority, string comments, string user)
        {
            RetrieveWarehouseLocations();

            if (!WarehouseLocations.Any())
            {
                LogOutput("No Warehouse Locations Available.");
                return false;
            }

            _stockTransferService.GenerateStockTransferID(user);

            foreach (string from in WarehouseLocations)
            {
                foreach (string to in WarehouseLocations)
                {
                    if (from != to)
                    {
                        try
                        {

                            _stockTransferService.RunStockTransfer(StockTransferId, from, to, priority, comments, user);

                        }
                        catch (Exception ex)
                        {
                            LogOutput(ex.Message);
                        }
                    }
                }
            }

            CompleteStockTransfer();
            LogOutput("Stock Transfer Complete!");
            return true;

        }


        /// <summary>
        /// Get Items To Transfer Per Location
        /// </summary>
        /// <param name="fromLocation">Location which has the most stock</param>
        /// <param name="toLocation">Location which is low on stock</param>
        /// <param name="priority">Priority of items to show. Leaving it blank will choose all priority items</param>
        /// <returns>A table of items to transfer</returns>
        public DataTable GetItemsToTransfer()
        {
            itemsToTransfer = _stockTransferRepository.GetItemsToTransfer(fromWarehouse, toWarehouse, priority);
            return itemsToTransfer;

        }

        /// <summary>
        /// Generate a Quote from Items To Transfer. 
        /// Note: Must have called GetItemsToTransfer(fromLocation, toLocation) at least once before.
        /// </summary>
        /// <param name="fromLocation">Location which has the most stock</param>
        /// <param name="toLocation">Location which is low on stock</param>
        /// <param name="priority">Priority of items. Leaving it blank will choose all priority items</param>
        /// <returns>Quote No</returns>
        public string GenerateQuote()
        {
            if (itemsToTransfer != null && itemsToTransfer.Rows.Count > 0)
            {
                return _stockTransferRepository.GenerateQuote(fromWarehouse, toWarehouse, priority);
            }
            throw new Exception("No items to transfer! Please check if you need to run GetItemsToTransfer() first");
        }
        
        /// <summary>
        /// Generate an Internal Requisition.
        /// Note: Must have called GenerateQuote() at least once before.
        /// </summary>
        /// <returns>Requisition number</returns>        
        public string GenerateRequisition()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Approve Step 2 Requisition. Must have called GenerateRequisition() at least once before.
        /// </summary>
        /// <returns>Approval</returns>
        public bool ApproveQty()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Approve Step 3 Requisition. Must have approved Step 2 Qty before.
        /// </summary>
        /// <returns>Approval</returns>
        public bool ApprovePrice()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Update quote with details of the generated PO.
        /// </summary>
        /// <returns>Updated outcome</returns>
        public bool UpdateQuote()
        {
            throw new NotImplementedException();
        }


        #region Helpers

        /// <summary>
        /// Get a list of all active warehouse locations for Stock Transfer
        /// </summary>
        /// <returns>A list of warehouse locations</returns>
        public List<string> RetrieveWarehouseLocations()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Generate an Internal Requisition from Quote.
        /// </summary>
        /// <param name="quoteNo">Quote to generate a requisition from.</param>
        /// <returns>Requisition number</returns>
        public string GenerateRequisition(string quoteNo)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Approve Step 2 Requisition.
        /// </summary>
        /// <param name="requisition">Requisition to approve. Must be Step 2.</param>
        /// <returns>Approval</returns>
        public bool ApproveQty(string requisition)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Approve Step 3 Requisition.
        /// </summary>
        /// <param name="requisition">Requisition to approve. Must be Step 3.</param>
        /// <returns>Approval</returns>
        public bool ApprovePrice(string requisition)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Update quote with details of the PO.
        /// </summary>
        /// <param name="quoteNo">Quote to update PO</param>
        /// <param name="fromLoc">Transfer from location</param>
        /// <param name="toLoc">Transfer to location</param>
        /// <param name="poNumber">PO number to include in Quote</param>
        /// <returns>Updated outcome</returns>
        public bool UpdateQuote(string quoteNo, string fromLoc, string toLoc, string poNumber)
        {
            string reference = poNumber + " (" + fromLoc.ToUpper() + " TO " + toLoc.ToUpper() + ")";
            return _stockTransferRepository.UpdateQuoteCustomerReference(quoteNo, reference);
        }

        /// <summary>
        /// Convert a quote to an order.
        /// </summary>
        /// <param name="quoteNo">Quote to convert.</param>
        /// <returns>Details of the converted order</returns>
        public DataTable ConvertQuoteToOrder(string quoteNo)
        {
            return _stockTransferRepository.ConvertQuoteToOrder(quoteNo);
        }

        #endregion

        #endregion

        #region Private Functions

        /// <summary>
        /// Generate new StockTransfer ID
        /// </summary>
        /// <returns></returns>
        private string GenerateNewStockTransfer()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds entry to Stock Transfer
        /// </summary>
        /// <param name="id">Stock Transfer ID</param>
        /// <returns>Entry line</returns>
        private string AddStockTransferEntry(string id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Store details of the stock transfer into the StockTransfer ID.
        /// </summary>
        private void CompleteStockTransfer()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Log to the database
        /// </summary>
        /// <param name="message"></param>
        private void LogOutput(string message)
        {
            //TODO: Log this somewhere
        }

        #endregion


    }
}
