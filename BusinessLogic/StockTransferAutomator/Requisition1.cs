﻿using Database.DataRepositories;
using Livingstone.Library;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.StockTransferAutomator
{
    public class Requisition1
    {
        //Private Variables
        private IRequisitionRepository _requisitionRepository;
        private bool hasAccessToFiles = false;
        private string requisitionNumber = "";
        private string requisitionType = "PO"; //formerly FormProcType
        private POBasket basket = new POBasket();
        private string supplierNo = "";
        private string supplierContactNo = "";
        private string supplierContactVendorNo = "";
        private int process_count = 1;
        private bool POPrinted = false;
        private int flag = 0;
        private bool statusOfPO = true;
        private bool isPOActive = true;
        private bool isProcessDone = false;
        private string step = "1";
        private int flagLivProcess = 0;
        private string assignByLiv = "";
        private bool canStillOpen = true;
        private bool validateTab = true;
        private decimal decNetTotal = 0;
        private bool isRequisitionOnly = false;

        private enum GroupAuthType { Authorization, Email, CreateGroup };

        //Constructor
        public Requisition1(IRequisitionRepository requisitionRepository)
        {
            _requisitionRepository = requisitionRepository;
        }

        //Public Fields
        public bool HasAccessToFiles { get => hasAccessToFiles; }
        public string SupplierContactNo { get => supplierContactNo; set => supplierContactNo = value; }
        public string SupplierContactVendorNo { get => supplierContactVendorNo; set => supplierContactVendorNo = value; }
        public string Step { get => step; set => step = value; }
        public bool CanStillOpen { get => canStillOpen; set => canStillOpen = value; }
        public string RequisitionNumber { get => requisitionNumber; set => requisitionNumber = value; }

        #region Public Functions
        //Public Functions
        public void LoadRequisition()
        {

        }

        public void LoadRequisition(double poNumber, double requisition, int flag2, int flag3, string type) //previously CallFormFunctions
        {
            CheckFilesAccess();
            requisitionType = type;

            bool isPoAuth = false, isPOPushed = false, CanOpenTheProcess = false, VariousReceivers = false, EnableSendBack = false;
            int AccStepNo = 0;

            DataTable reqDetails = _requisitionRepository.GetRequisitionHeaderDetails(requisition.ToString());

            string strOrd = reqDetails.Rows[0]["ordernr"].ToString();
            this.SupplierContactNo = reqDetails.Rows[0]["contact_number"].ToString();
            this.SupplierContactVendorNo = reqDetails.Rows[0]["vend_no"].ToString();
            poNumber = Convert.ToDouble(strOrd.Trim() == string.Empty ? "0" : strOrd.Trim());

            if (reqDetails.Rows.Count > 0)
            {
                string tempTask = reqDetails.Rows[0]["TaskNo"].ToString().Trim();
                // use a big integer 98/99 to represent PO step C/H in order to simplify step no comparison. e.g. process_count > 5
                process_count = tempTask == "C" ? 98 : (tempTask == "H" ? 99 : Convert.ToInt32(tempTask));
                AccStepNo = Convert.ToInt16(reqDetails.Rows[0]["acc_step"].ToString());
            }

            POPrinted = _requisitionRepository.POPrinted(poNumber.ToString());

            //use this logic to make sure that users who have the Role will not be able to process the PO if they don't have the required permission
            if (flag3 == 1)
            {
                if (process_count == 3)
                {
                    if (!PermissionReader.HasPermission("POCanAuth"))
                    {
                        flag3 = 0;
                        VariousReceivers = true;
                    }
                }
                if (process_count == 4)
                {
                    if (!PermissionReader.HasPermission("CanCreatePOXML"))
                    {
                        flag3 = 0;
                        VariousReceivers = true;
                    }
                }
            }

            try
            {
                if (requisitionType == "PO")
                {
                    DateTime t1 = DateTime.Now;
                    flag = flag2;

                    DataTable dt = _requisitionRepository.GetPOLivProcess(requisition.ToString());
                    DataTable dt2 = _requisitionRepository.GetPOToDoRole(requisition.ToString());

                    string CheckStatus = (string)reqDetails.Rows[0]["taskNo"];

                    //CanStillOpen = CheckIfFormIsOpen(req); //check if form is currently opened by another user

                    int tester = 0;
                    if (CheckStatus.Trim() == "C") //if PO is already cancelled
                    {
                        statusOfPO = false;
                       
                        isPOActive = false;

                        isProcessDone = true;
                    }
                    else //if PO is still active
                    {
                        if (dt.Rows.Count > 0 || Step == "1") //if still in Processes
                        {
                            if (dt.Rows.Count > 0)
                            {
                                flagLivProcess = Convert.ToInt16(dt.Rows[0]["flag"].ToString().Trim());
                                assignByLiv = dt.Rows[0]["assign_by"].ToString().Trim();
                                CanOpenTheProcess = CheckIfCanOpenProcess("P", dt, requisition);
                            }
                            if (flag3 != 0)
                            {
                                dt = _requisitionRepository.GetPOLivProcessForAssignedUser(requisition.ToString(), PermissionReader.LoginName);
                                if (dt.Rows.Count <= 0) CanStillOpen = false;
                            }
                        }
                        else
                        {
                            if (dt2.Rows.Count > 0) //if still in Group Task
                            {
                                flagLivProcess = Convert.ToInt16(dt2.Rows[0]["flag"].ToString().Trim());
                                assignByLiv = dt2.Rows[0]["assign_by"].ToString().Trim();
                                CanOpenTheProcess = CheckIfCanOpenProcess("GT", dt2, requisition);
                                if (flag3 != 0)
                                {
                                    dt = _requisitionRepository.GetPOToDoRoleWithAssignRoles(requisition, new string[] { "POGroup", "POAccGroup" });
                                    if (dt.Rows.Count <= 0) CanStillOpen = false;
                                }
                                //if (dt2.Rows[0]["step"].ToString().Trim() == "0" && flag_3 != 0)
                                if (dt2.Rows[0]["step"].ToString().Trim() == "0")
                                {
                                    //DisableHeaderTab();
                                    isProcessDone = true;
                                }
                                if (dt2.Rows[0]["step"].ToString().Trim() == "1")
                                {

                                    isPoAuth = true;
                                }
                            }
                            else //if already pushed to Exact
                            {

                                isPOPushed = true;
                                isProcessDone = true; //used in CheckControlEnability function
                                string msg = "This PO has been created.";
                                dt = _requisitionRepository.GetExactOrder(poNumber.ToString().Trim()); 
               
                                EnableSendBack = process_count <= 6 ? true : false;
                            }
                        }
                    }

                    if (CanStillOpen) tester++;
                    if (flag3 == 0) //used in PO and Requisition Search; if flag3 is 1 that means it's from Work Centre
                    {
                        if (!VariousReceivers)
                        {
                            if (CanOpenTheProcess)
                            {
                                if (!isPoAuth) //enable controls for the recipient eventhough it's in Requisition search
                                {
                                    CanStillOpen = tester > 0 ? true : false;

                                }
                            }
                            else //for the person who searched the requisition but still wants to edit it as long as not yet opened by the receiver
                            {
                                CanStillOpen = (PermissionReader.LoginName == assignByLiv && flagLivProcess == 1 && !isPoAuth && tester > 0) ? true : false;
                            }
                        }
                        else CanStillOpen = false;
                    }

                    if (CanStillOpen)
                    {
                        _requisitionRepository.UpdateFormOpenLog(requisition, true, "PO", PermissionReader.LoginName);
                    }

                //    TOFIX
                //    LoadPO(poNo, req);

                //    SetDataGridPO();
                //    SetPayments();
                //    SetFreight();
                //    SetBudget();
                //    SetPOFiles();

                //    LoadGrid(poNo, req);
                //    DateTime t2 = DateTime.Now;
                //    decNetTotal = decimal.Parse(txtNetTotal.Text);
                //    if (poNumber != 0)
                //    {
                //        _requisitionRepository.AddToLivProcessHistory(RequisitionNumber, PermissionReader.GetUserFromPermission("POCanAuth"));

                //    }
                //    if (EnableSendBack)
                //    {
                //        int UserCounter = 0;
                //        //string PermissionGroup = location.Trim().ToUpper() == "Z" ? "POAccGroup" : "POGroup";
                //        string PermissionGroup = GroupType(location, GroupAuthType.CreateGroup);

                //        // Modified by Leomar Rejano. October 7, 2010.
                //        // Purpose: Change the query. Remove the condition: b.company = 'company name'
                //        DataTable dtReceivers = _requisitionRepository.GetDistinctUsersWithRole(PermissionGroup.Trim());

                //        if (dtReceivers.Rows.Count > 0)
                //        {
                //            foreach (DataRow dr in dtReceivers.Rows)
                //            {
                //                if (dr["usr"].ToString().Trim() == PermissionReader.LoginName) UserCounter++;
                //            }
                //        }

                //        //if (PermissionReader.HasPermission("POCanAuth") || UserCounter > 0 && process_count <= 6)
                //        //    btnSendBack.Visible = btnSendBack.Enabled = true;
                //    }

                //    //this will check and open PP through Requisition Search and other means except Work Centre
                //    if ((IsMemberOfGroup("POPayment") || IsMemberOfGroup("POFreight") || PermissionReader.LoginName == BuyerPlannerName) && (Step == "5" || Step == "6" || Step == "7") && !rbAcc.Checked)
                //    {
                //        requisitionType = "PP";
                //        CanStillOpen = CheckIfFormIsOpen(requisition);
                //        //btnCompletePay.Visible = btnCompletePay.Enabled = CanStillOpen;
                //        if (CanStillOpen)
                //        {
                //            _requisitionRepository.UpdateFormOpenLog(requisition, true, "PP", PermissionReader.LoginName);


                //            CheckDefault();
                //            SetPaymentGroupControls(false);
                //        }
                //    }


                //}
                //else
                //{
                //    CanStillOpen = CheckIfFormIsOpen(requisition);
                //    if (CanStillOpen)
                //    {
                //        _requisitionRepository.UpdateFormOpenLog(requisition, true, "PP", PermissionReader.LoginName);
                //    }
                //    //DisableControls(req, false);
                //    LoadPO(poNo, req);
                //    SetPayments();
                //    SetFreight();
                //    SetBudget();
                //    SetPOFiles();
                //    LoadGrid(poNo, req);
                //    //CreateAccountPaymentProcess();

                //    string PP_Step = _requisitionRepository.GetAccountStepFromRequisition(RequisitionNumber);
                //    //if ((rbPre.Checked || rbTD.Checked || rbTP.Checked) && IsVendorForeign && PP_Step == "5")
                //    //{
                //    //    PaymentFreightControlsEnability(true);
                //    //    txtPDesc.Text = "Complete Freight Details";
                //    //}
                //    //else txtPDesc.Text = "Complete Payment Details";

                //    //btnCompletePay.Visible = btnCompletePay.Enabled = CanStillOpen;

                //    if (Step.Trim() != "H")
                //    {
                //        isRequisitionOnly = true;
                //        //chkReqOnly.Enabled = true;
                //        //dteExpiryDate.Enabled = true;
                //    }
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            validateTab = true;
        }

        //use this function to determine if the user can open/modify the requisition, either from Work Centre or Requisition Search
        public bool CheckIfCanOpenProcess(string type, DataTable dt, double req)
        {
            int Success = 0;
            DataTable dtGroup = new DataTable();
            string loc = _requisitionRepository.GetLocationFromRequisition(req);

            if (type.Trim().ToUpper() == "P")
            {
                if (dt.Rows.Count > 0)
                {
                    if (process_count == 2)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["assign_to"].ToString().Trim() == PermissionReader.LoginName) Success++;
                        }
                    }
                    else
                    {
                        //string AssignRole = loc.Trim().ToUpper() == "Z" ? "POAccAuthorization" : "POAuthorization";
                        string AssignRole = GroupType(loc, GroupAuthType.Authorization);

                        dtGroup = _requisitionRepository.GetUsersWithRole(AssignRole.Trim());
                        if (dtGroup.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dtGroup.Rows)
                            {
                                if (dr["usr"].ToString().Trim() == PermissionReader.LoginName) Success++;
                            }
                        }
                        else
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (dr["assign_to"].ToString().Trim() == PermissionReader.LoginName) Success++;
                            }
                        }
                    }
                    if (Success > 0) _requisitionRepository.UnflagRequisitionProcess(req.ToString());
                }
            }
            if (type.Trim().ToUpper() == "GT") //GT means Group Task
            {
                if (dt.Rows.Count > 0)
                {
                    string AssignRole = _requisitionRepository.GetAssignRoleFromRequisition(req.ToString());
                    dtGroup = _requisitionRepository.GetUsersWithRole(AssignRole.Trim());
                    if (dtGroup.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtGroup.Rows)
                        {
                            if (dr["usr"].ToString().Trim() == PermissionReader.LoginName) Success++;
                        }
                    }
                    if (Success > 0) _requisitionRepository.UnflagRequisitionToDo(req.ToString());
                }
            }

            return Success > 0 ? true : false;
        }

#endregion

        #region Private functions
        //Private functions
        private void CheckFilesAccess()
        {
            hasAccessToFiles = PermissionReader.HasPermission("POFileOpen");
        }

        private bool isAccount(string loc)
        {
            List<string> list = new List<string>(new string[] { "Z", "TZ", "VZ", "ZTZ", "ZMZ", "ZM", "ZVZ", "M", "UM", "UZ", "ZUZ", "ZSYZ" });

            return list.Contains(loc);
            //return loc == "Z" || loc == "TZ" || loc == "VZ" || loc == "ZTZ" || loc == "ZMZ" || loc == "ZVZ";
        }

        private string GroupType(string locationstring, GroupAuthType group)
        {
            string warehouseLocation = locationstring.Trim().ToUpper();
            string groupType = "";

            // Accounting Group
            if (isAccount(warehouseLocation))
            {
                switch (group)
                {
                    case GroupAuthType.Authorization:
                        groupType = "POAccAuthorization";
                        break;
                    case GroupAuthType.Email:
                        groupType = "POAccEmailGroup";
                        break;
                    case GroupAuthType.CreateGroup:
                        groupType = "POAccGroup";
                        break;
                }
            }
            // Purchasing Group
            else
            {
                switch (group)
                {
                    case GroupAuthType.Authorization:

                        bool isNeedQtyAuth = false;

                        if (basket != null && basket.DataTable != null && basket.DataTable.Rows.Count > 0 && (Step.Trim() == "1" || Step.Trim() == "2"))
                        {
                            foreach (DataRow dr in basket.DataTable.Rows)
                            {
                                if (dr["isQtyNeedAuth"].ToString().Trim() == "1" &&
                                dr["QtyAuthBy"].ToString().Trim() == "")
                                {
                                    isNeedQtyAuth = true;
                                    break;
                                }

                            }

                        }
                        if (isNeedQtyAuth)
                        {
                            bool isLocal = true;

                            if (supplierNo.Trim() != "")
                            {
                                string countrycode = _requisitionRepository.GetCountryCodeOfSupplier(supplierNo);
                                if (!countrycode.ToUpper().StartsWith("AU")) isLocal = false;
                            }
                            string buyer_p = "";
                            if (RequisitionNumber.Trim() != "" && RequisitionNumber.Trim() != "0")
                                buyer_p = _requisitionRepository.GetBuyerPlannerFromRequisition(RequisitionNumber);


                            if (isLocal)
                            {
                                groupType = "POQtyConfirmLocal";
                            }

                            else
                            {
                                groupType = "POQtyConfirmForeign";
                            }

                        }
                        else
                        {
                            groupType = "POAuthorization";

                        }

                        break;
                    case GroupAuthType.Email:
                        groupType = "POemailGroup";
                        break;
                    case GroupAuthType.CreateGroup:
                        groupType = "POGroup";
                        break;
                }
            }

            return groupType;
        }

        /// <summary>
        /// Checks if the current form is in use. If it is being used then it will give the user the option of unlocking it.
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        private bool CheckIfFormIsOpen(double req)
        {
            //// Check if PO is currently open.
            //if (!CheckIfFormIsOpenAuxiliary(req))
            //{
            //    try
            //    {
            //        ClsChild frm = CrossProjectFunctionCalls.CallOpenHelperForm(new CrossProjectFunctionCalls.FormCmd("532", DataSource.getConn("NETCRMAU"))
            //            , req.ToString(), true, null);
            //        frm.ShowDialog();
            //    }
            //    catch
            //    {
            //        MessageBox.Show((FormProcType.Trim() == "PO" ? "Requisition " : "Payment Process ") + "is currently open.", "NetCRM", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    }
            //}

            // Return second check to see if user changed unlocked the PO via the Helper entry.
            return CheckIfFormIsOpenAuxiliary(req);
        }

        private bool CheckIfFormIsOpenAuxiliary(double req)
        {
            string msg = string.Empty;
            DataTable dt = _requisitionRepository.CheckFormOpenLog(req, requisitionType);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(dt.Rows[i]["isOpen"]))
                        msg = dt.Rows[i]["user"].ToString().Trim();
                }
            }
            if (msg != string.Empty)
            {
                return false;
            }
            return true;
        }

        #endregion
    }
}
