﻿using BusinessLogic.StockTransferAutomator;
using Database.DataRepositories;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Data;

namespace Tests.Requisition.Tests
{
    [TestFixture]
    public class RequisitionTests
    {
        private IRequisitionRepository _requisitionRepositoryMock;
        private IStockTransferRepository _stockTransferRepositoryMock;

        [OneTimeSetUp]
        public void Setup()
        {
            _requisitionRepositoryMock = Substitute.For<IRequisitionRepository>();
            _stockTransferRepositoryMock = Substitute.For<IStockTransferRepository>();

        }

        [Test]
        public void VerifyGetItemsToTransferIsValid()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ItemToTransfer");
            dt.Columns.Add("UOM");
            dt.Columns.Add("WarehouseTransferFrom");
            dt.Columns.Add("WarehouseTransferTo");
            dt.Columns.Add("CartonQtyToTransfer");
            dt.Columns.Add("CBMToTransfer");
            dt.Columns.Add("LatestETADate");
            dt.Columns.Add("ActualQtyToTransfer");
            dt.Rows.Add("LWMS", "BX", "R", "T", "8", "0.1", "17/09/2018 1:12 PM", "80");

            _stockTransferRepositoryMock.GetItemsToTransfer("", "", "").ReturnsForAnyArgs(dt);
            StockTransferAutomator automator = new StockTransferAutomator(_requisitionRepositoryMock, _stockTransferRepositoryMock, null);

            Assert.AreEqual(dt, automator.GetItemsToTransfer());
        }

        [Test]
        public void VerifyGenerateQuoteIsValid()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ItemToTransfer");
            dt.Columns.Add("UOM");
            dt.Columns.Add("WarehouseTransferFrom");
            dt.Columns.Add("WarehouseTransferTo");
            dt.Columns.Add("CartonQtyToTransfer");
            dt.Columns.Add("CBMToTransfer");
            dt.Columns.Add("LatestETADate");
            dt.Columns.Add("ActualQtyToTransfer");
            dt.Rows.Add("LWMS", "BX", "R", "T", "8", "0.1", "17/09/2018 1:12 PM", "80");

            _stockTransferRepositoryMock.GetItemsToTransfer("", "", "").ReturnsForAnyArgs(dt);
            _stockTransferRepositoryMock.GenerateQuote("R", "T", "").ReturnsForAnyArgs("111111");
            StockTransferAutomator automator = new StockTransferAutomator(_requisitionRepositoryMock, _stockTransferRepositoryMock, null);

            Assert.Throws<Exception>(() => automator.GenerateQuote());

            automator.GetItemsToTransfer();

            Assert.AreEqual("111111", automator.GenerateQuote());
        }

        [Test]
        public void VerifyCanUpdateCustomerReference()
        {
            _stockTransferRepositoryMock.UpdateQuoteCustomerReference("", "").ReturnsForAnyArgs(true);

            StockTransferAutomator automator = new StockTransferAutomator(_requisitionRepositoryMock, _stockTransferRepositoryMock, null);

            Assert.IsTrue(automator.UpdateQuote("123456", "R", "T", "7890"));
        }

        [Test]
        public void VerifyCanConvertQuote()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ExactOrderNo");
            dt.Columns.Add("NetCRMOrderNo");
            dt.Columns.Add("Status");
            dt.Columns.Add("Location");
            dt.Rows.Add("312334", "012344", "2", "R");

            DataTable dt2 = null;
            _stockTransferRepositoryMock.ConvertQuoteToOrder("123456").Returns(dt);
            _stockTransferRepositoryMock.ConvertQuoteToOrder("789012").Returns(dt2);

            StockTransferAutomator automator = new StockTransferAutomator(_requisitionRepositoryMock, _stockTransferRepositoryMock, null);
            //automator.Quote = "123456";
            //Assert.AreEqual(dt, automator.ConvertQuoteToOrder());

            //automator.Quote = "789012";
            //Assert.AreEqual(dt2, automator.ConvertQuoteToOrder());

            //automator.Quote = "";
            //Assert.Throws<Exception>(() => automator.ConvertQuoteToOrder());

        }
    }
}
