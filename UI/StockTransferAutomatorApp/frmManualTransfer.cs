﻿using Autofac;
using BusinessLogic.StockTransferAutomator;
using BusinessLogic.StockTransferAutomator.Services;
using Database.DatabaseRepositories.Model;
using Database.DataRepositories;
using System;
using System.Data;
using System.Windows.Forms;

namespace UI.StockTransferAutomatorApp
{
    public partial class frmManualTransfer : Form
    {
        private StockTransferAutomator automator;
        private IStockTransferService stockTransferService;

        public frmManualTransfer()
        {
            InitializeComponent();

            //StockTransferRepository stockTransferRepository = new StockTransferRepository();
            //RequisitionRepository requisitionRepository = new RequisitionRepository();


            //automator = new StockTransferAutomator(requisitionRepository, stockTransferRepository, null);
            using (var scope = Program.BuildContainer().BeginLifetimeScope())
            {
                stockTransferService = scope.Resolve<IStockTransferService>();
                cboFromLocation.DataSource = stockTransferService.RetrieveAllWarehouses();
                cboToLocation.DataSource = stockTransferService.RetrieveAllWarehouses();
            }
        }

        private void btnGetItemsToTransfer_Click(object sender, EventArgs e)
        {
            string fromWarehouse = (cboFromLocation.SelectedItem != null ? cboFromLocation.SelectedItem.ToString().Trim() : "");
            string toWarehouse = (cboToLocation.SelectedItem != null ? cboToLocation.SelectedItem.ToString().Trim() : "");
            string priority = (cboPriority.SelectedItem != null ? cboPriority.SelectedItem.ToString().Trim() : "");

            if (fromWarehouse == toWarehouse)
            {
                MessageBox.Show("Cannot select the same warehouse.");
                return;
            }

            DataTable itemsToTransfer = stockTransferService.GetItemsToTransfer(fromWarehouse, toWarehouse, priority);
            dgrItemsToTransfer.DataSource = itemsToTransfer;
        }

        private void btnGenerateQuote_Click(object sender, EventArgs e)
        {
            SaveStockTransferEntry("DANANT");
            string fromWarehouse = (cboFromLocation.SelectedItem != null ? cboFromLocation.SelectedItem.ToString().Trim() : "");
            string toWarehouse = (cboToLocation.SelectedItem != null ? cboToLocation.SelectedItem.ToString().Trim() : "");
            DataTable items = (DataTable)dgrItemsToTransfer.DataSource;

            if (fromWarehouse == toWarehouse)
            {
                MessageBox.Show("Cannot select the same warehouse.");
                return;
            }
            if (items == null || items.Rows.Count <= 0)
            {
                MessageBox.Show("Need at least 1 item to transfer.");
                return;
            }

            

            string quote = stockTransferService.GenerateQuote(txtStockTransferID.Text, "", (DataTable)dgrItemsToTransfer.DataSource);
            txtQuote.Text = quote;
        }

        private void btnGenerateRequisition_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Function not available yet! Coming soon.");
        }

        private void btnApproveQty_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Function not available yet! Coming soon.");

        }

        private void btnApprovePrice_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Function not available yet! Coming soon.");

        }

        private void btnUpdateQuote_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Function not available yet! Coming soon.");

        }

        private void btnConvertQuote_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Function not available yet! Coming soon.");

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveStockTransferEntry("DANANT");
        }

        private void btnPasteFromExcel_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Function not available yet! Coming soon.");

        }

        private void btnRunStockTransfer_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Function not available yet! Will be added later.");
        }

        private void SaveStockTransferEntry(string user)
        {
            string fromWarehouse = (cboFromLocation.SelectedItem != null ? cboFromLocation.SelectedItem.ToString().Trim() : "");
            string toWarehouse = (cboToLocation.SelectedItem != null ? cboToLocation.SelectedItem.ToString().Trim() : "");
            string priority = (cboPriority.SelectedItem != null ? cboPriority.SelectedItem.ToString().Trim() : "");
            DataTable items = (DataTable)dgrItemsToTransfer.DataSource;

            if (items == null || items.Rows.Count <= 0)
            {
                MessageBox.Show("Cannot save. Need at least 1 item in the grid.");
                return;
            }

            txtStockTransferID.Text = stockTransferService.SaveEntry(txtStockTransferID.Text, fromWarehouse, toWarehouse, priority, user,
                txtQuote.Text, txtNetCRMOrderNo.Text, txtExactOrderNo.Text, txtRequisition.Text, txtPONumber.Text, items);
        }

        private void btnLoadFromID_Click(object sender, EventArgs e)
        {
            StockTransferEntry entry = stockTransferService.GetEntryHeader(txtLoadID.Text);

            txtStockTransferID.Text = entry.StockTransferID;

            txtQuote.Text = entry.QuoteNo;
            txtNetCRMOrderNo.Text = entry.NetcrmOrderNo;
            txtExactOrderNo.Text = entry.ExactOrderNo;

            txtRequisition.Text = entry.Requisition;
            txtPONumber.Text = entry.PONumber;

            cboFromLocation.Text = entry.FromWarehouse;
            cboToLocation.Text = entry.ToWarehouse;
            cboPriority.Text = entry.Priority;

            DataTable lines = stockTransferService.GetEntryLines(txtLoadID.Text);

            dgrItemsToTransfer.DataSource = lines;
        }
    }
}
