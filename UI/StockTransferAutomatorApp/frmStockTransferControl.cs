﻿using Database.DataRepositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.StockTransferAutomatorApp
{
    public partial class frmStockTransferControl : Form
    {
        public frmStockTransferControl()
        {
            InitializeComponent();
        }

        private void btnManualTransfer_Click(object sender, EventArgs e)
        {
            frmManualTransfer manualTransfer = new frmManualTransfer();
            manualTransfer.Show();
        }

        private void btnViewHistory_Click(object sender, EventArgs e)
        {
            NotReady();
        }

        private void NotReady()
        {
            MessageBox.Show("Not available yet! Coming Soon.");
        }

        private void btnManageSTCustomer_Click(object sender, EventArgs e)
        {
            frmMaintainCustomer maintainCustomer = new frmMaintainCustomer();
            maintainCustomer.ShowDialog();
        }

        private void btnManageWHPriority_Click(object sender, EventArgs e)
        {
            NotReady();
        }
    }
}
