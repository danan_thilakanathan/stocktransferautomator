﻿using Autofac;
using BusinessLogic.StockTransferAutomator.Services;
using Database.DataRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.StockTransferAutomatorApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var container = BuildContainer();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            using (var scope = container.BeginLifetimeScope())
            {
                Application.Run(new frmStockTransferControl());
            }
        }

        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();
            //builder.RegisterType<EmployeeRepository>().As<IEmployeeRepository>();
            //builder.RegisterType<EmployeeService>();
            //return builder.Build();

            var assemblies = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(assemblies)
                   .Where(t => t.Name.EndsWith("Repository")).AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(assemblies)
                   .Where(t => t.Name.EndsWith("Service")).AsImplementedInterfaces();

            builder.RegisterType<StockTransferService>().As<IStockTransferService>();
            builder.RegisterType<RequisitionService>().As<IRequisitionService>();
            builder.RegisterType<StockTransferRepository>().As<IStockTransferRepository>();
            builder.RegisterType<RequisitionRepository>().As<IRequisitionRepository>();

            builder.RegisterType<GenerateQuoteService>().As<IGenerateQuoteService>();
            builder.RegisterType<GetItemsToTransferService>().As<IGetItemsToTransferService>();
            builder.RegisterType<LoadRequisitionFromQuoteService>().As<ILoadRequisitionFromQuoteService>();



            return builder.Build();
        }
    }
}
