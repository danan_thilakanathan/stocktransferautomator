﻿namespace UI.StockTransferAutomatorApp
{
    partial class frmManualTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmManualTransfer));
            this.cboFromLocation = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboPriority = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboToLocation = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgrItemsToTransfer = new System.Windows.Forms.DataGridView();
            this.txtExactOrderNo = new System.Windows.Forms.TextBox();
            this.btnGetItemsToTransfer = new System.Windows.Forms.Button();
            this.txtStep = new System.Windows.Forms.TextBox();
            this.txtNetCRMOrderNo = new System.Windows.Forms.TextBox();
            this.txtQuote = new System.Windows.Forms.TextBox();
            this.txtPONumber = new System.Windows.Forms.TextBox();
            this.txtRequisition = new System.Windows.Forms.TextBox();
            this.gpbItems = new System.Windows.Forms.GroupBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnRunStockTransfer = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtStockTransferID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnConvertQuote = new System.Windows.Forms.Button();
            this.btnUpdateQuote = new System.Windows.Forms.Button();
            this.btnApprovePrice = new System.Windows.Forms.Button();
            this.btnApproveQty = new System.Windows.Forms.Button();
            this.btnGenerateRequisition = new System.Windows.Forms.Button();
            this.btnGenerateQuote = new System.Windows.Forms.Button();
            this.gpbFilter = new System.Windows.Forms.GroupBox();
            this.gpbMore = new System.Windows.Forms.GroupBox();
            this.chkTestOnly = new System.Windows.Forms.CheckBox();
            this.btnLoadFromID = new System.Windows.Forms.Button();
            this.btnPasteFromExcel = new System.Windows.Forms.Button();
            this.txtLoadID = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgrItemsToTransfer)).BeginInit();
            this.gpbItems.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gpbFilter.SuspendLayout();
            this.gpbMore.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // cboFromLocation
            // 
            this.cboFromLocation.FormattingEnabled = true;
            this.cboFromLocation.Location = new System.Drawing.Point(45, 22);
            this.cboFromLocation.Name = "cboFromLocation";
            this.cboFromLocation.Size = new System.Drawing.Size(77, 21);
            this.cboFromLocation.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Exact Ord No:";
            // 
            // cboPriority
            // 
            this.cboPriority.FormattingEnabled = true;
            this.cboPriority.Location = new System.Drawing.Point(283, 22);
            this.cboPriority.Name = "cboPriority";
            this.cboPriority.Size = new System.Drawing.Size(72, 21);
            this.cboPriority.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(59, 140);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Step:";
            // 
            // cboToLocation
            // 
            this.cboToLocation.FormattingEnabled = true;
            this.cboToLocation.Location = new System.Drawing.Point(160, 22);
            this.cboToLocation.Name = "cboToLocation";
            this.cboToLocation.Size = new System.Drawing.Size(72, 21);
            this.cboToLocation.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(49, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "PO No:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(236, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Priority:";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "NetCRM Ord No:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "From:";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Requisition No:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(131, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "To:";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Quote No:";
            // 
            // dgrItemsToTransfer
            // 
            this.dgrItemsToTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrItemsToTransfer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrItemsToTransfer.Location = new System.Drawing.Point(5, 19);
            this.dgrItemsToTransfer.Name = "dgrItemsToTransfer";
            this.dgrItemsToTransfer.Size = new System.Drawing.Size(816, 352);
            this.dgrItemsToTransfer.TabIndex = 1;
            // 
            // txtExactOrderNo
            // 
            this.txtExactOrderNo.Location = new System.Drawing.Point(97, 55);
            this.txtExactOrderNo.Name = "txtExactOrderNo";
            this.txtExactOrderNo.Size = new System.Drawing.Size(100, 20);
            this.txtExactOrderNo.TabIndex = 5;
            // 
            // btnGetItemsToTransfer
            // 
            this.btnGetItemsToTransfer.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnGetItemsToTransfer.Location = new System.Drawing.Point(381, 17);
            this.btnGetItemsToTransfer.Name = "btnGetItemsToTransfer";
            this.btnGetItemsToTransfer.Size = new System.Drawing.Size(243, 29);
            this.btnGetItemsToTransfer.TabIndex = 0;
            this.btnGetItemsToTransfer.Text = "Get Items To Transfer";
            this.btnGetItemsToTransfer.UseVisualStyleBackColor = true;
            this.btnGetItemsToTransfer.Click += new System.EventHandler(this.btnGetItemsToTransfer_Click);
            // 
            // txtStep
            // 
            this.txtStep.Location = new System.Drawing.Point(97, 133);
            this.txtStep.Name = "txtStep";
            this.txtStep.Size = new System.Drawing.Size(100, 20);
            this.txtStep.TabIndex = 5;
            // 
            // txtNetCRMOrderNo
            // 
            this.txtNetCRMOrderNo.Location = new System.Drawing.Point(97, 29);
            this.txtNetCRMOrderNo.Name = "txtNetCRMOrderNo";
            this.txtNetCRMOrderNo.Size = new System.Drawing.Size(100, 20);
            this.txtNetCRMOrderNo.TabIndex = 5;
            // 
            // txtQuote
            // 
            this.txtQuote.Location = new System.Drawing.Point(97, 3);
            this.txtQuote.Name = "txtQuote";
            this.txtQuote.Size = new System.Drawing.Size(100, 20);
            this.txtQuote.TabIndex = 5;
            // 
            // txtPONumber
            // 
            this.txtPONumber.Location = new System.Drawing.Point(97, 107);
            this.txtPONumber.Name = "txtPONumber";
            this.txtPONumber.Size = new System.Drawing.Size(100, 20);
            this.txtPONumber.TabIndex = 5;
            // 
            // txtRequisition
            // 
            this.txtRequisition.Location = new System.Drawing.Point(97, 81);
            this.txtRequisition.Name = "txtRequisition";
            this.txtRequisition.Size = new System.Drawing.Size(100, 20);
            this.txtRequisition.TabIndex = 5;
            // 
            // gpbItems
            // 
            this.gpbItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpbItems.Controls.Add(this.btnSave);
            this.gpbItems.Controls.Add(this.btnRunStockTransfer);
            this.gpbItems.Controls.Add(this.tableLayoutPanel1);
            this.gpbItems.Controls.Add(this.dgrItemsToTransfer);
            this.gpbItems.Controls.Add(this.txtStockTransferID);
            this.gpbItems.Controls.Add(this.label9);
            this.gpbItems.Location = new System.Drawing.Point(0, 95);
            this.gpbItems.Name = "gpbItems";
            this.gpbItems.Size = new System.Drawing.Size(1039, 377);
            this.gpbItems.TabIndex = 8;
            this.gpbItems.TabStop = false;
            this.gpbItems.Text = "Items To Transfer";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(827, 292);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRunStockTransfer
            // 
            this.btnRunStockTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRunStockTransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRunStockTransfer.Location = new System.Drawing.Point(827, 324);
            this.btnRunStockTransfer.Name = "btnRunStockTransfer";
            this.btnRunStockTransfer.Size = new System.Drawing.Size(198, 47);
            this.btnRunStockTransfer.TabIndex = 9;
            this.btnRunStockTransfer.Text = "Run Stock Transfer";
            this.btnRunStockTransfer.UseVisualStyleBackColor = true;
            this.btnRunStockTransfer.Click += new System.EventHandler(this.btnRunStockTransfer_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtQuote, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtNetCRMOrderNo, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtExactOrderNo, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtStep, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtRequisition, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtPONumber, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(827, 123);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(200, 163);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // txtStockTransferID
            // 
            this.txtStockTransferID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStockTransferID.Enabled = false;
            this.txtStockTransferID.Location = new System.Drawing.Point(933, 19);
            this.txtStockTransferID.Name = "txtStockTransferID";
            this.txtStockTransferID.Size = new System.Drawing.Size(100, 20);
            this.txtStockTransferID.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(835, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Stock Transfer ID:";
            // 
            // btnConvertQuote
            // 
            this.btnConvertQuote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvertQuote.Location = new System.Drawing.Point(838, 3);
            this.btnConvertQuote.Name = "btnConvertQuote";
            this.btnConvertQuote.Size = new System.Drawing.Size(198, 47);
            this.btnConvertQuote.TabIndex = 9;
            this.btnConvertQuote.Text = "Convert Quote";
            this.btnConvertQuote.UseVisualStyleBackColor = true;
            this.btnConvertQuote.Click += new System.EventHandler(this.btnConvertQuote_Click);
            // 
            // btnUpdateQuote
            // 
            this.btnUpdateQuote.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnUpdateQuote.Location = new System.Drawing.Point(664, 3);
            this.btnUpdateQuote.Name = "btnUpdateQuote";
            this.btnUpdateQuote.Size = new System.Drawing.Size(138, 47);
            this.btnUpdateQuote.TabIndex = 10;
            this.btnUpdateQuote.Text = "Update Quote";
            this.btnUpdateQuote.UseVisualStyleBackColor = true;
            this.btnUpdateQuote.Click += new System.EventHandler(this.btnUpdateQuote_Click);
            // 
            // btnApprovePrice
            // 
            this.btnApprovePrice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnApprovePrice.Location = new System.Drawing.Point(479, 3);
            this.btnApprovePrice.Name = "btnApprovePrice";
            this.btnApprovePrice.Size = new System.Drawing.Size(145, 47);
            this.btnApprovePrice.TabIndex = 11;
            this.btnApprovePrice.Text = "Approve Price";
            this.btnApprovePrice.UseVisualStyleBackColor = true;
            this.btnApprovePrice.Click += new System.EventHandler(this.btnApprovePrice_Click);
            // 
            // btnApproveQty
            // 
            this.btnApproveQty.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnApproveQty.Location = new System.Drawing.Point(335, 3);
            this.btnApproveQty.Name = "btnApproveQty";
            this.btnApproveQty.Size = new System.Drawing.Size(102, 47);
            this.btnApproveQty.TabIndex = 12;
            this.btnApproveQty.Text = "Approve Qty";
            this.btnApproveQty.UseVisualStyleBackColor = true;
            this.btnApproveQty.Click += new System.EventHandler(this.btnApproveQty_Click);
            // 
            // btnGenerateRequisition
            // 
            this.btnGenerateRequisition.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGenerateRequisition.Location = new System.Drawing.Point(147, 3);
            this.btnGenerateRequisition.Name = "btnGenerateRequisition";
            this.btnGenerateRequisition.Size = new System.Drawing.Size(145, 47);
            this.btnGenerateRequisition.TabIndex = 13;
            this.btnGenerateRequisition.Text = "Generate Requisition";
            this.btnGenerateRequisition.UseVisualStyleBackColor = true;
            this.btnGenerateRequisition.Click += new System.EventHandler(this.btnGenerateRequisition_Click);
            // 
            // btnGenerateQuote
            // 
            this.btnGenerateQuote.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGenerateQuote.Location = new System.Drawing.Point(3, 3);
            this.btnGenerateQuote.Name = "btnGenerateQuote";
            this.btnGenerateQuote.Size = new System.Drawing.Size(104, 47);
            this.btnGenerateQuote.TabIndex = 14;
            this.btnGenerateQuote.Text = "Generate Quote";
            this.btnGenerateQuote.UseVisualStyleBackColor = true;
            this.btnGenerateQuote.Click += new System.EventHandler(this.btnGenerateQuote_Click);
            // 
            // gpbFilter
            // 
            this.gpbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpbFilter.Controls.Add(this.gpbMore);
            this.gpbFilter.Controls.Add(this.btnGetItemsToTransfer);
            this.gpbFilter.Controls.Add(this.cboFromLocation);
            this.gpbFilter.Controls.Add(this.label1);
            this.gpbFilter.Controls.Add(this.cboPriority);
            this.gpbFilter.Controls.Add(this.label2);
            this.gpbFilter.Controls.Add(this.cboToLocation);
            this.gpbFilter.Controls.Add(this.label10);
            this.gpbFilter.Location = new System.Drawing.Point(0, 2);
            this.gpbFilter.Name = "gpbFilter";
            this.gpbFilter.Size = new System.Drawing.Size(1039, 87);
            this.gpbFilter.TabIndex = 15;
            this.gpbFilter.TabStop = false;
            // 
            // gpbMore
            // 
            this.gpbMore.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.gpbMore.Controls.Add(this.chkTestOnly);
            this.gpbMore.Controls.Add(this.btnLoadFromID);
            this.gpbMore.Controls.Add(this.btnPasteFromExcel);
            this.gpbMore.Controls.Add(this.txtLoadID);
            this.gpbMore.Location = new System.Drawing.Point(632, 10);
            this.gpbMore.Name = "gpbMore";
            this.gpbMore.Size = new System.Drawing.Size(401, 71);
            this.gpbMore.TabIndex = 5;
            this.gpbMore.TabStop = false;
            this.gpbMore.Text = "More Options";
            // 
            // chkTestOnly
            // 
            this.chkTestOnly.AutoSize = true;
            this.chkTestOnly.Location = new System.Drawing.Point(315, 48);
            this.chkTestOnly.Name = "chkTestOnly";
            this.chkTestOnly.Size = new System.Drawing.Size(82, 17);
            this.chkTestOnly.TabIndex = 6;
            this.chkTestOnly.Text = "Is Test Only";
            this.chkTestOnly.UseVisualStyleBackColor = true;
            // 
            // btnLoadFromID
            // 
            this.btnLoadFromID.Location = new System.Drawing.Point(298, 19);
            this.btnLoadFromID.Name = "btnLoadFromID";
            this.btnLoadFromID.Size = new System.Drawing.Size(97, 23);
            this.btnLoadFromID.TabIndex = 0;
            this.btnLoadFromID.Text = "Load From ID";
            this.btnLoadFromID.UseVisualStyleBackColor = true;
            this.btnLoadFromID.Click += new System.EventHandler(this.btnLoadFromID_Click);
            // 
            // btnPasteFromExcel
            // 
            this.btnPasteFromExcel.Location = new System.Drawing.Point(6, 19);
            this.btnPasteFromExcel.Name = "btnPasteFromExcel";
            this.btnPasteFromExcel.Size = new System.Drawing.Size(107, 23);
            this.btnPasteFromExcel.TabIndex = 0;
            this.btnPasteFromExcel.Text = "Paste From Excel";
            this.btnPasteFromExcel.UseVisualStyleBackColor = true;
            this.btnPasteFromExcel.Click += new System.EventHandler(this.btnPasteFromExcel_Click);
            // 
            // txtLoadID
            // 
            this.txtLoadID.Location = new System.Drawing.Point(192, 21);
            this.txtLoadID.Name = "txtLoadID";
            this.txtLoadID.Size = new System.Drawing.Size(100, 20);
            this.txtLoadID.TabIndex = 5;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 11;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.btnGenerateQuote, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnConvertQuote, 10, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnUpdateQuote, 8, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnApprovePrice, 6, 0);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox2, 9, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnApproveQty, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnGenerateRequisition, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox4, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox5, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox6, 7, 0);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox3, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 478);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1039, 53);
            this.tableLayoutPanel2.TabIndex = 16;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(808, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 28);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(298, 12);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(31, 28);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 17;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(443, 12);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(30, 28);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 18;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(630, 12);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(28, 28);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 15;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(113, 12);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(28, 28);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 16;
            this.pictureBox3.TabStop = false;
            // 
            // frmManualTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1041, 543);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.gpbFilter);
            this.Controls.Add(this.gpbItems);
            this.Name = "frmManualTransfer";
            this.Text = "Manual Transfer";
            ((System.ComponentModel.ISupportInitialize)(this.dgrItemsToTransfer)).EndInit();
            this.gpbItems.ResumeLayout(false);
            this.gpbItems.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.gpbFilter.ResumeLayout(false);
            this.gpbFilter.PerformLayout();
            this.gpbMore.ResumeLayout(false);
            this.gpbMore.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboFromLocation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboPriority;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboToLocation;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgrItemsToTransfer;
        private System.Windows.Forms.TextBox txtExactOrderNo;
        private System.Windows.Forms.Button btnGetItemsToTransfer;
        private System.Windows.Forms.TextBox txtStep;
        private System.Windows.Forms.TextBox txtNetCRMOrderNo;
        private System.Windows.Forms.TextBox txtQuote;
        private System.Windows.Forms.TextBox txtPONumber;
        private System.Windows.Forms.TextBox txtRequisition;
        private System.Windows.Forms.GroupBox gpbItems;
        private System.Windows.Forms.Button btnConvertQuote;
        private System.Windows.Forms.Button btnUpdateQuote;
        private System.Windows.Forms.Button btnApprovePrice;
        private System.Windows.Forms.Button btnApproveQty;
        private System.Windows.Forms.Button btnGenerateRequisition;
        private System.Windows.Forms.Button btnGenerateQuote;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gpbFilter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox gpbMore;
        private System.Windows.Forms.Button btnPasteFromExcel;
        private System.Windows.Forms.TextBox txtStockTransferID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnLoadFromID;
        private System.Windows.Forms.TextBox txtLoadID;
        private System.Windows.Forms.Button btnRunStockTransfer;
        private System.Windows.Forms.CheckBox chkTestOnly;
    }
}