﻿namespace UI.StockTransferAutomatorApp
{
    partial class frmStockTransferControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnManageSTCustomer = new System.Windows.Forms.Button();
            this.btnManageWHPriority = new System.Windows.Forms.Button();
            this.gbpSettings = new System.Windows.Forms.GroupBox();
            this.btnViewHistory = new System.Windows.Forms.Button();
            this.btnManualTransfer = new System.Windows.Forms.Button();
            this.gbpSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnManageSTCustomer
            // 
            this.btnManageSTCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnManageSTCustomer.Location = new System.Drawing.Point(9, 28);
            this.btnManageSTCustomer.Name = "btnManageSTCustomer";
            this.btnManageSTCustomer.Size = new System.Drawing.Size(359, 50);
            this.btnManageSTCustomer.TabIndex = 0;
            this.btnManageSTCustomer.Text = "Manage Stock Transfer Customer";
            this.btnManageSTCustomer.UseVisualStyleBackColor = true;
            this.btnManageSTCustomer.Click += new System.EventHandler(this.btnManageSTCustomer_Click);
            // 
            // btnManageWHPriority
            // 
            this.btnManageWHPriority.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnManageWHPriority.Location = new System.Drawing.Point(9, 84);
            this.btnManageWHPriority.Name = "btnManageWHPriority";
            this.btnManageWHPriority.Size = new System.Drawing.Size(359, 50);
            this.btnManageWHPriority.TabIndex = 0;
            this.btnManageWHPriority.Text = "Manage Warehouse Priority";
            this.btnManageWHPriority.UseVisualStyleBackColor = true;
            this.btnManageWHPriority.Click += new System.EventHandler(this.btnManageWHPriority_Click);
            // 
            // gbpSettings
            // 
            this.gbpSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbpSettings.Controls.Add(this.btnManageSTCustomer);
            this.gbpSettings.Controls.Add(this.btnManageWHPriority);
            this.gbpSettings.Location = new System.Drawing.Point(12, 187);
            this.gbpSettings.Name = "gbpSettings";
            this.gbpSettings.Size = new System.Drawing.Size(378, 174);
            this.gbpSettings.TabIndex = 1;
            this.gbpSettings.TabStop = false;
            this.gbpSettings.Text = "Manage Settings";
            // 
            // btnViewHistory
            // 
            this.btnViewHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewHistory.Location = new System.Drawing.Point(13, 92);
            this.btnViewHistory.Name = "btnViewHistory";
            this.btnViewHistory.Size = new System.Drawing.Size(377, 62);
            this.btnViewHistory.TabIndex = 2;
            this.btnViewHistory.Text = "View History";
            this.btnViewHistory.UseVisualStyleBackColor = true;
            this.btnViewHistory.Click += new System.EventHandler(this.btnViewHistory_Click);
            // 
            // btnManualTransfer
            // 
            this.btnManualTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnManualTransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManualTransfer.Location = new System.Drawing.Point(13, 24);
            this.btnManualTransfer.Name = "btnManualTransfer";
            this.btnManualTransfer.Size = new System.Drawing.Size(377, 62);
            this.btnManualTransfer.TabIndex = 2;
            this.btnManualTransfer.Text = "Manual Transfer";
            this.btnManualTransfer.UseVisualStyleBackColor = true;
            this.btnManualTransfer.Click += new System.EventHandler(this.btnManualTransfer_Click);
            // 
            // frmStockTransferControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 373);
            this.Controls.Add(this.btnManualTransfer);
            this.Controls.Add(this.btnViewHistory);
            this.Controls.Add(this.gbpSettings);
            this.Name = "frmStockTransferControl";
            this.Text = "Stock Transfer Control";
            this.gbpSettings.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnManageSTCustomer;
        private System.Windows.Forms.Button btnManageWHPriority;
        private System.Windows.Forms.GroupBox gbpSettings;
        private System.Windows.Forms.Button btnViewHistory;
        private System.Windows.Forms.Button btnManualTransfer;
    }
}

