﻿using BusinessLogic.StockTransferAutomator.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.StockTransferAutomatorApp
{
    public partial class frmMaintainCustomer : Form
    {
        private IStockTransferService stockTransferService;

        public frmMaintainCustomer()
        {
            InitializeComponent();

            //stockTransferService = new StockTransferService();

            txtCustomer.Text = stockTransferService.GetInternalCustomer();
        }

        private void btnUpdateCustomer_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCustomer.Text.Trim()))
            {
                MessageBox.Show("Need to enter a customer code");
                return;
            }

            stockTransferService.UpdateCustomer(txtCustomer.Text.Trim().ToUpper());

            MessageBox.Show("Updated!");
            this.Close();
        }
    }
}
