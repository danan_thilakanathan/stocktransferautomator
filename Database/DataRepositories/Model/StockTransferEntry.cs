﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DatabaseRepositories.Model
{
    public class StockTransferEntry
    {
        public string StockTransferID { get; set; }
        public string FromWarehouse { get; set; }
        public string ToWarehouse { get; set; }
        public string Priority { get; set; }
        public string QuoteNo { get; set; }
        public string NetcrmOrderNo { get; set; }
        public string ExactOrderNo { get; set; }
        public string Requisition { get; set; }
        public string PONumber { get; set; }
        public DataTable Items { get; set; }
        public string User { get; set; }
    }
}
