﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataRepositories
{
    public interface IUserRepository
    {
        DataTable GetUserInformation(string username);
        DataTable RetrievePermission(string username);
        DataTable RetrieveRole(string username);

    }
}
