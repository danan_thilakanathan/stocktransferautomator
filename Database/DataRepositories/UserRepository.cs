﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataRepositories
{
    public class UserRepository : IUserRepository
    {
        private dbConnection con;

        public UserRepository()
        {
            con = new dbConnection();
        }


        public DataTable GetUserInformation(string username)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@username", SqlDbType.VarChar) { Value = username };

                DataTable dt = con.executeSelectQuery(@"
                    SELECT DISTINCT * from [user]
                    WHERE usr = @username", parameters);

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public DataTable RetrievePermission(string username)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@username", SqlDbType.VarChar) { Value = username };

                DataTable dt = con.executeSelectQuery(@"
                    SELECT a.permission, b.screen, b.name, b.office
                    FROM UserPermission a
	                    LEFT OUTER JOIN [user] b ON a.usr = b.usr 
                    WHERE a.usr = @username AND a.PermissionValue = 1 ", parameters);

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public DataTable RetrieveRole(string username)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@username", SqlDbType.VarChar) { Value = username };

                DataTable dt = con.executeSelectQuery(@"
                    SELECT role 
                    FROM userrole a
	                    LEFT OUTER JOIN [user] b ON a.usr = b.usr
                    WHERE a.usr = @username", parameters);

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
    }
}
