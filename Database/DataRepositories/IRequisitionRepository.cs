﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataRepositories
{
    public interface IRequisitionRepository
    {
        DataTable GetRequisitionHeaderDetails(string requisition);
        bool POPrinted(string poNumber);
        DataTable GetPOLivProcess(string requisition);
        DataTable GetPOToDoRole(string requisition);
        DataTable GetPOLivProcessForAssignedUser(string requisition, string loginName);
        DataTable GetPOToDoRoleWithAssignRoles(double requisition, string[] roles);
        void UnflagRequisitionProcess(string requisition);
        void UnflagRequisitionToDo(string v);
        DataTable GetUsersWithRole(string v);
        string GetAssignRoleFromRequisition(string v);
        DataTable GetQuoteLinesFromQuote(string quote);
        string GetLocationFromRequisition(double req);
        DataTable GetExactOrder(string orderNo);
        string GetBuyerPlannerFromRequisition(string requisitionNumber);
        string GetCountryCodeOfSupplier(string supplierNo);
        void UpdateFormOpenLog(double requisition, bool isOpen, string processType, string loginName);
        void AddToLivProcessHistory(string requisitionNumber, string user);
        string GetAccountStepFromRequisition(string requisitionNumber);
        DataTable CheckFormOpenLog(double req, string requisitionType);
        DataTable GetDistinctUsersWithRole(string v);
        DataTable GetItemDataFromVendor(string itemNo, string supplierNo);
        decimal GetHandlingCharge(string supplierNo);
    }
}
