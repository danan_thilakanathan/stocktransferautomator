﻿using Database.DatabaseRepositories.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataRepositories
{
    public interface IStockTransferRepository
    {
        bool UpdateQuoteCustomerReference(string quoteNo, string reference);
        DataTable ConvertQuoteToOrder(string quoteNo);
        string GenerateQuote(string fromLocation, string toLocation, string priority);
        string GenerateQuote(string stockTransferId, string comments, DataTable items);
        DataTable GetItemsToTransfer(string fromWarehouse, string toWarehouse, string priority);
        StockTransferEntry GetEntryHeader(string stockTransferID);
        DataTable GetEntryLines(string stockTransferID);
        string AddStockTransferEntry(string stockTransferId, string user);
        string SaveEntry(StockTransferEntry entry);
        List<string> RetrieveAllWarehouses();
        string GenerateStockTransferID(string user);
        bool UpdateInternalCustomer(string customer);
        string GetInternalCustomer();
    }
}
