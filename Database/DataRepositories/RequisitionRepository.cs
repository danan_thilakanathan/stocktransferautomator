﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataRepositories
{
    public class RequisitionRepository : IRequisitionRepository
    {
        private SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["NetCRMAU"].ConnectionString);

        public void AddToLivProcessHistory(string requisitionNumber, string user)
        {
            throw new NotImplementedException();
        }

        public DataTable CheckFormOpenLog(double req, string requisitionType)
        {
            throw new NotImplementedException();
        }

        public string GetAccountStepFromRequisition(string requisitionNumber)
        {
            throw new NotImplementedException();
        }

        public string GetAssignRoleFromRequisition(string v)
        {
            throw new NotImplementedException();
        }

        public string GetBuyerPlannerFromRequisition(string requisitionNumber)
        {
            throw new NotImplementedException();
        }

        public string GetCountryCodeOfSupplier(string supplierNo)
        {
            throw new NotImplementedException();
        }

        public DataTable GetDistinctUsersWithRole(string v)
        {
            throw new NotImplementedException();
        }

        public DataTable GetExactOrder(string orderNo)
        {
            throw new NotImplementedException();
        }

        public decimal GetHandlingCharge(string supplierNo)
        {
            try
            {
                if (!string.IsNullOrEmpty(supplierNo))
                {
                    var dataSet = new DataSet();

                    SqlCommand cmd = con.CreateCommand();

                    cmd.CommandText = @"
                        SELECT tb2.handlingchg
                        FROM apother tb1
                        INNER JOIN apordinfo tb2
                        ON tb1.otherid = tb2.otherid
                        WHERE tb1.vendor_no = @supplierNo";
                    cmd.Parameters.Add(new SqlParameter("@supplierNo", SqlDbType.VarChar) { Value = supplierNo });

                    var dataAdapter = new SqlDataAdapter { SelectCommand = cmd };

                    dataAdapter.Fill(dataSet);

                    decimal handlingCharge;
                    if (decimal.TryParse(dataSet.Tables[0].Rows[0][0].ToString(), out handlingCharge))
                    {
                        return handlingCharge;
                    }
                    else
                        return 0;
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }
        }

        public DataTable GetItemDataFromVendor(string itemNo, string supplierNo)
        {
            try
            {
                con.Open();

                var dataSet = new DataSet();

                SqlCommand cmd = con.CreateCommand();

                cmd.CommandText = @"
                    SELECT * FROM poitmvnd_sql WHERE item_no = @itemNo AND vend_no = @supplierNo";
                cmd.Parameters.Add(new SqlParameter("@itemNo", SqlDbType.VarChar) { Value = itemNo });
                cmd.Parameters.Add(new SqlParameter("@supplierNo", SqlDbType.VarChar) { Value = supplierNo });

                var dataAdapter = new SqlDataAdapter { SelectCommand = cmd };

                dataAdapter.Fill(dataSet);

                return dataSet.Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }
        }

        public string GetLocationFromRequisition(double req)
        {
            throw new NotImplementedException();
        }

        public DataTable GetPOLivProcess(string requisition)
        {
            throw new NotImplementedException();
        }

        public DataTable GetPOLivProcessForAssignedUser(string requisition, string loginName)
        {
            throw new NotImplementedException();
        }

        public DataTable GetPOToDoRole(string requisition)
        {
            throw new NotImplementedException();
        }

        public DataTable GetPOToDoRoleWithAssignRoles(double requisition, string[] roles)
        {
            throw new NotImplementedException();
        }

        public DataTable GetQuoteLinesFromQuote(string quote)
        {
            throw new NotImplementedException();
        }

        public DataTable GetRequisitionHeaderDetails(string requisition)
        {
            throw new NotImplementedException();
        }

        public DataTable GetUsersWithRole(string v)
        {
            throw new NotImplementedException();
        }

        public bool POPrinted(string poNumber)
        {
            throw new NotImplementedException();
        }

        public void UnflagRequisitionProcess(string requisition)
        {
            throw new NotImplementedException();
        }

        public void UnflagRequisitionToDo(string v)
        {
            throw new NotImplementedException();
        }

        public void UpdateFormOpenLog(double requisition, bool isOpen, string processType, string loginName)
        {
            throw new NotImplementedException();
        }
    }
}
