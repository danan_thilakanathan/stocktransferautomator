﻿using Database.DatabaseRepositories.Model;
using Livingstone.OrderLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataRepositories
{
    public class StockTransferRepository : IStockTransferRepository
    {
        private dbConnection con;

        public StockTransferRepository()
        {
            con = new dbConnection();
        }

        public StockTransferEntry GetEntryHeader(string stockTransferID)
        {
            try
            {

                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@stockTransferID", SqlDbType.VarChar) { Value = stockTransferID };

                DataTable dt = con.executeSelectQuery(@"
                    select * from tblStockTransferEntry
                    where StockTransferID = @stockTransferID", parameters ); 

                StockTransferEntry entry = new StockTransferEntry();
                entry.StockTransferID = dt.Rows[0]["StockTransferID"].ToString();
                entry.FromWarehouse = dt.Rows[0]["FromLocation"].ToString();
                entry.ToWarehouse = dt.Rows[0]["ToLocation"].ToString();
                entry.Priority = dt.Rows[0]["Priority"].ToString();
                entry.User = dt.Rows[0]["UserCreated"].ToString();
                entry.QuoteNo = dt.Rows[0]["QuoteNo"].ToString();
                entry.NetcrmOrderNo = dt.Rows[0]["NetCRMOrderNo"].ToString();
                entry.ExactOrderNo = dt.Rows[0]["ExactOrderNo"].ToString();
                entry.Requisition = dt.Rows[0]["Requisition"].ToString();
                entry.PONumber = dt.Rows[0]["PONumber"].ToString();

                return entry;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
  
        }

        public DataTable GetEntryLines(string stockTransferID)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@stockTransferID", SqlDbType.VarChar) { Value = stockTransferID };

                DataTable dt = con.executeSelectQuery(@"
                    select ItemNo, UOM, WarehouseFrom, WarehouseTo, [Priority], CartonQtyToTransfer, CBMToTransfer, LatestETADate, ActualQtyToTransfer, IncludeInTransfer 
                    from tblStockTransferItemsToTransfer
                    where StockTransferID = @stockTransferID", parameters);

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

        }

        public string SaveEntry(StockTransferEntry entry)
        {
            try
            {

                if (string.IsNullOrEmpty(entry.StockTransferID)) entry.StockTransferID = "-1";

                con.storeTempTable(@"CREATE TABLE #ItemsToTransfer (
                    ItemNo varchar(50),
                    UOM varchar(10),
                    WarehouseFrom varchar(10),
                    WarehouseTo varchar(10),
                    [Priority] varchar(10),
                    CartonQtyToTransfer int,
                    CBMToTransfer decimal(18, 6),
                    LatestETADate datetime,
                    ActualQtyToTransfer int,
                    IncludeInTransfer bit
                )", "#ItemsToTransfer", entry.Items);

                SqlParameter[] parameters = new SqlParameter[10];
                parameters[0] = new SqlParameter("@stockTransferID", SqlDbType.VarChar) { Value = entry.StockTransferID };
                parameters[1] = new SqlParameter("@fromLocation", SqlDbType.VarChar) { Value = entry.FromWarehouse };
                parameters[2] = new SqlParameter("@toLocation", SqlDbType.VarChar) { Value = entry.ToWarehouse };
                parameters[3] = new SqlParameter("@priority", SqlDbType.VarChar) { Value = entry.Priority };
                parameters[4] = new SqlParameter("@quoteNo", SqlDbType.VarChar) { Value = entry.QuoteNo };
                parameters[5] = new SqlParameter("@netCRMOrderNo", SqlDbType.VarChar) { Value = entry.NetcrmOrderNo };
                parameters[6] = new SqlParameter("@exactOrderNo", SqlDbType.VarChar) { Value = entry.ExactOrderNo };
                parameters[7] = new SqlParameter("@requisition", SqlDbType.VarChar) { Value = entry.Requisition };
                parameters[8] = new SqlParameter("@poNumber", SqlDbType.VarChar) { Value = entry.PONumber };
                parameters[9] = new SqlParameter("@usr", SqlDbType.VarChar) { Value = entry.User };

                DataTable dt = con.executeSelectQuery(@"
                    exec spStockTransferSaveEntry @stockTransferID, @fromLocation, @toLocation, 
	                                            @priority, @quoteNo, @netCRMOrderNo, @exactOrderNo, @requisition, @poNumber, @usr", parameters);

                return dt.Rows[0][0].ToString();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

        }

        public string AddStockTransferEntry(string stockTransferId, string user)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@stockTransferID", SqlDbType.VarChar) { Value = stockTransferId };
                parameters[1] = new SqlParameter("@userName", SqlDbType.VarChar) { Value = user };

                DataTable dt = con.executeSelectQuery(@"
                    exec spStockTransferGenerateEntry @stockTransferID, @userName", parameters);

                return dt.Rows[0]["entryID"].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }


        }

        public DataTable ConvertQuoteToOrder(string quoteNo)
        {
            LivQuote quote = new LivQuote("0");
            quote.LoadQuote(quoteNo);
            quote.CreateOrders();

            return quote.ExactOrders;
        }

        public string GenerateQuote(string fromLocation, string toLocation, string priority)
        {
            // exec[GenerateQuoteItemList4_ForInternalTransfersOnly] 'R', 'T', '', 'DANANT'

            throw new NotImplementedException();
        }

        public string GenerateQuote(string stockTransferId, string comments, DataTable items)
        {
            try
            {
                con.storeTempTable(@"CREATE TABLE #ItemsToTransfer (
                    ItemNo varchar(50),
                    UOM varchar(10),
                    WarehouseFrom varchar(10),
                    WarehouseTo varchar(10),
                    [Priority] varchar(10),
                    CartonQtyToTransfer int,
                    CBMToTransfer decimal(18, 6),
                    latestETADate datetime,
                    ActualQtyToTransfer int,
                    IncludedInTransfer bit
                )", "#ItemsToTransfer", items);

                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@stockTransferId", SqlDbType.VarChar) { Value = stockTransferId };
                parameters[1] = new SqlParameter("@comments", SqlDbType.VarChar) { Value = comments };

                DataTable dt = con.executeSelectQuery(@"
                    exec[GenerateQuoteItemList4_ForInternalTransfersOnly_ItemsOnly_ByEntryID] @stockTransferId, @comments", parameters);

                return dt.Rows[0]["quoteno"].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

        }

        public DataTable GetItemsToTransfer(string fromWarehouse, string toWarehouse, string priority)
        {
            // spStockTransferItemsToTransfer 'R', 'T', ''

            try
            {
                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@from", SqlDbType.VarChar) { Value = fromWarehouse };
                parameters[1] = new SqlParameter("@to", SqlDbType.VarChar) { Value = toWarehouse };
                parameters[2] = new SqlParameter("@priority", SqlDbType.VarChar) { Value = priority };

                DataTable dt = con.executeSelectQuery(@"
                    exec spStockTransferItemsToTransfer @from, @to, @priority", parameters);

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

        }

        public bool UpdateQuoteCustomerReference(string quoteNo, string reference)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@reference", SqlDbType.VarChar) { Value = reference };
                parameters[1] = new SqlParameter("@quoteNo", SqlDbType.VarChar) { Value = quoteNo };


                DataTable dt = con.executeSelectQuery(@"
                    update quotehdr set CustomerRef = @reference where quoteNo = @quoteNo", parameters);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public List<string> RetrieveAllWarehouses()
        {

            try
            {
                DataTable dt = con.executeSelectQuery(@"
                    select distinct warehouse from tblStockTransferWarehousePriority");
 

                return dt.AsEnumerable()
                            .Select(r => r.Field<string>(0))
                            .ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public string GenerateStockTransferID(string user)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@user", SqlDbType.VarChar) { Value = user };

                DataTable dt = con.executeSelectQuery(@"
                    exec spStockTransferGenerateHeader @user", parameters);

                return dt.Rows[0]["ID"].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public bool UpdateInternalCustomer(string customer)
        {
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@customer", SqlDbType.VarChar) { Value = customer };

                DataTable dt = con.executeSelectQuery(@"
                    update tblStockTransferCustomer set Customer = @customer", parameters);

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public string GetInternalCustomer()
        {
            try
            {
                DataTable dt = con.executeSelectQuery(@"
                    select Customer from tblStockTransferCustomer");

                return dt.Rows[0]["Customer"].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
    }
}
